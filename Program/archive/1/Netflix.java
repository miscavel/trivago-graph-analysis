import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class Netflix
{
    private static float[] constants;
    private static float[][] relationships;
    private static int movieID, userID, rating, targetID, userCount;
    private static float targetRating, learningRate, rms;
    private static float[] predictions;
    private static int[] idArr;
    private static List<Integer> movIDList = new ArrayList<Integer>(), movRatingList = new ArrayList<Integer>();

    public static class TokenizerMapper extends Mapper<Object, Text, IntWritable, Text>
    {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[];

            if (line.contains(":"))
            {
                movieID = Integer.parseInt(line.substring(0, line.indexOf(":")));
            }
            else if (line.contains(","))
            {
                data = line.split(",");
                userID = Integer.parseInt(data[0]);
                rating = Integer.parseInt(data[1]);
            }
            context.write(new IntWritable(userID), new Text(Integer.toString(movieID) + "|" + Integer.toString(rating)));
        }
    }

    public static class RegressionMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            userCount++;
            String line = value.toString();
            String ratings;
            String data[];
            targetRating = 0;

            data = line.split("\\t");
            userID = Integer.parseInt(data[0]);
            ratings = data[1];
            data = data[1].split(",");
            for (int i = 0; i < data.length; i++)
            {
                if (data[i].matches(targetID + "\\|.*"))
                {
                    targetRating = Float.parseFloat(data[i].split("\\|")[1]);
                    //System.out.println(data[i]);
                }
            }
            for (int i = 0; i < data.length; i++)
            {
                context.write(new Text(Integer.toString(userID) + "-" + Float.toString(targetRating) + "-" + ratings), new Text(data[i]));
            }
        }
    }

    public static class IntSumReducer extends Reducer<IntWritable,Text,IntWritable,Text>
    {
        private IntWritable result = new IntWritable();

        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String temp = "";
            for (Text val : values)
            {
                temp += val.toString() + ",";
            }
            temp = temp.substring(0, temp.length() - 1);
            context.write(key, new Text(temp));
        }
    }

    public static class RegressionReducer extends Reducer<Text,Text,Text,Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            float total = 0, temp = 0;
            String keystr = key.toString();
            String keydat[] = keystr.split("-");
            targetRating = Float.parseFloat(keydat[1]);

            for (Text val : values)
            {
                String data[] = val.toString().split("\\|");
                //System.out.println(val.toString());
                try
                {
                    if (!data[0].equals(Integer.toString(targetID)))
                    {
                        temp = Float.parseFloat(data[1]) * constants[Integer.parseInt(data[0]) - 1];
                    }
                    else
                    {
                        temp = 0;
                    }
                    //temp = Float.parseFloat(data[1]);
                }
                catch (Exception e)
                {
                    //System.out.println(e);
                    temp = targetRating - Float.parseFloat(val.toString());
                }
                total += temp;
            }
            total = targetRating - total;

            context.write(key, new Text(Float.toString(total)));
        }

        @Override
        protected void cleanup(Context context) throws IOException, InterruptedException
        {
            System.out.println("Finished");
        }
    }

    public static class ErrorMapper extends Mapper<Object, Text, IntWritable, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[];
            String temp[];
            float diff;

            data = line.split("\\t");
            diff = Float.parseFloat(data[1]);

            rms += (Math.pow(diff, 2.0) / (2 * userCount));

            data = data[0].split("-");
            data = data[2].split(",");
            for (int i = 0; i < data.length; i++)
            {
                temp = data[i].split("\\|");
                movieID = Integer.parseInt(temp[0]);
                rating = Integer.parseInt(temp[1]);
                diff = (rating * diff * learningRate) / userCount;
                if (movieID != targetID)
                {
                    context.write(new IntWritable(movieID), new Text(Float.toString(diff)));
                }
            }
        }
    }

    public static class ErrorReducer extends Reducer<IntWritable,Text,IntWritable,Text>
    {
        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            float total = 0;
            for (Text val : values)
            {
                total += Float.parseFloat(val.toString());
            }

            constants[key.get() - 1] += total;

            context.write(key, new Text(Float.toString(total)));
        }
    }

    public static class RatingMapper extends Mapper<Object, Text, IntWritable, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split("\\t");
            context.write(new IntWritable(Integer.parseInt(data[0])), new Text(data[1]));
        }
    }

    public static class RatingReducer extends Reducer<IntWritable,Text,IntWritable,Text>
    {
        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            System.out.println(key);
            String line, result = "";
            String data[], bundle[];
            int movID, movRating;
            movIDList.clear();
            movRatingList.clear();
            float total;
            boolean skip = false;

            for (int i = 0; i < predictions.length; i++)
            {
                predictions[i] = 0;
                idArr[i] = (i + 1);
            }

            for (Text val : values)
            {
                line = val.toString();
                if (line.contains(":"))
                {
                    result = line;
                    skip = true;
                }
                if (!skip)
                {
                    data = line.split(",");
                    for (int i = 0; i < data.length; i++)
                    {
                        bundle = data[i].split("\\|");
                        movID = Integer.parseInt(bundle[0]);
                        movRating = Integer.parseInt(bundle[1]);
                        predictions[movID - 1] = -100;
                        movIDList.add(movID);
                        movRatingList.add(movRating);
                    }
                }
            }

            if (!skip)
            {
                for (int i = 0; i < predictions.length; i++)
                {
                    total = 0;
                    if (predictions[i] == 0)
                    {
                        if ((i + 1) == targetID) // y Prediction
                        {
                            for (int j = 0; j < movIDList.size(); j++)
                            {
                                if (movIDList.get(j) != (i + 1))
                                {
                                    total += movRatingList.get(j) * constants[movIDList.get(j) - 1];
                                }
                            }
                        }
                        else // Inverse Prediction for x(s)
                        {
                            for (int j = 0; j < movIDList.size(); j++)
                            {
                                //System.out.println(movRatingList.get(j) + " - " + (movRatingList.get(j) * constants[movIDList.get(j) - 1]));
                                if (movIDList.get(j) != (i + 1))
                                {
                                    if (movIDList.get(j) == targetID)
                                    {
                                        total += movRatingList.get(j) * constants[movIDList.get(j) - 1] * relationships[i][movIDList.get(j) - 1];
                                    }
                                    else
                                    {
                                        total -= movRatingList.get(j) * constants[movIDList.get(j) - 1] * relationships[i][movIDList.get(j) - 1];
                                    }
                                }
                            }
                        }
                        //predictions[i] = (total / constants[i]);

                        /*if (Math.abs(constants[i]) > Math.pow((1.0 / movieID), 2.0))
                        {
                            predictions[i] = (total / constants[i]);
                        }
                        else
                        {
                            predictions[i] = 0;
                        }*/
                        predictions[i] = (constants[i] < 0) ? (-1 * total) : total;
                        //System.out.println(predictions[i]);
                        result += (i + 1) + ":" + predictions[i];
                        if (i < predictions.length - 1)
                        {
                            result += ",";
                        }
                    }
                }
                float temp;
                for (int i = 0; i < predictions.length; i++)
                {
                    for (int j = i; j < predictions.length; j++)
                    {
                        if (predictions[j] > predictions[i])
                        {
                            temp = predictions[i];
                            predictions[i] = predictions[j];
                            predictions[j] = temp;

                            temp = idArr[i];
                            idArr[i] = idArr[j];
                            idArr[j] = (int)temp;
                        }
                    }
                }
                SaveRecommendations(key.get(), idArr, predictions);
            }
            context.write(key, new Text(result));
        }
    }

    public static class RelationshipMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[];
            String row[];
            String col[];
            float distance;
            int rowID, colID;

            data = line.split("\\t");
            data = data[1].split(",");
            for (int i = 0; i < data.length; i++)
            {
                //System.out.println(data[i]);
                row = data[i].split("\\|");
                rowID = Integer.parseInt(row[0]);
                for (int j = 0; j < i; j++)
                {
                    //System.out.println(data[j]);
                    col = data[j].split("\\|");
                    colID = Integer.parseInt(col[0]);
                    distance = Float.parseFloat(row[1]) - Float.parseFloat(col[1]);
                    try
                    {
                        if (distance != 0)
                        {
                            relationships[rowID - 1][colID - 1] += 1 / (Math.pow(distance, 2.0));
                        }
                        else
                        {
                            relationships[rowID - 1][colID - 1] += 1;
                        }
                        relationships[rowID - 1][colID - 1] /= 2;
                        relationships[colID - 1][rowID - 1] = relationships[rowID - 1][colID - 1];
                    }
                    catch (Exception e)
                    {
                        System.out.println(e);
                        System.out.println(rowID + " " + colID);
                        System.out.println(data[i] + " " + data[j]);
                    }
                }
            }
        }
    }

    public static class RelationshipReducer extends Reducer<Text,Text,Text,Text>
    {
        public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {

        }
    }

    public static void SaveRecommendations(int key, int id[], float data[]) throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter("recommendation.txt", true));
        writer.append(key + "\t");
        for (int i = 0; i < 10; i++)
        {
            //writer.append(Integer.toString(id[i]) + ":" + Float.toString(data[i]));
            writer.append(Integer.toString(id[i]));
            if (i < 9)
            {
                writer.append(",");
            }
        }
        writer.newLine();
        writer.close();
    }

    public static void SaveConstants() throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter("constants.txt"));

        for (int i = 0; i < constants.length; i++)
        {
            writer.write(Float.toString(constants[i]));
            writer.newLine();
        }

        writer.close();
    }

    public static void LoadConstants() throws IOException
    {
        String line;
        int count = 0;
        BufferedReader reader = new BufferedReader(new FileReader("constants.txt"));
        while((line = reader.readLine()) != null)
        {
            constants[count] = Float.parseFloat(line);
            count++;
        }
        reader.close();
    }

    public static void SaveError() throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter("error.txt", true));
        writer.append(Float.toString(rms));
        writer.newLine();
        writer.close();
    }

    public static void SaveRelationships() throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter("relationships.txt"));

        for (int i = 0; i < relationships.length; i++)
        {
            for (int j = 0; j < relationships[i].length; j++)
            {
                writer.write(Float.toString(relationships[i][j]));
                if (j < relationships[i].length - 1)
                {
                    writer.write(";");
                }
            }
            writer.newLine();
        }
        writer.close();
    }

    public static void LoadRelationships() throws IOException
    {
        String line;
        String data[];
        int count = 0;
        BufferedReader reader = new BufferedReader(new FileReader("relationships.txt"));
        while((line = reader.readLine()) != null)
        {
            data = line.split(";");
            for (int i = 0; i < data.length; i++)
            {
                relationships[count][i] = Float.parseFloat(data[i]);
            }
            count++;
        }
        reader.close();
    }

    public static void main(String[] args) throws Exception
    {
        int iteration = 10;
        targetID = 1;
        userCount = 0;
        learningRate = 2f;
        Configuration conf = new Configuration();
        FileSystem hdfs = FileSystem.get(conf);
        Job job;

        /*job = Job.getInstance(conf, "Grouping");

        //Mapping Data
        job.setJarByClass(Netflix.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("NetflixInput"));

        if (hdfs.exists(new Path("NetflixOutput")))
        {
            hdfs.delete(new Path("NetflixOutput"), true);
        }

        FileOutputFormat.setOutputPath(job, new Path("NetflixOutput"));

        job.waitForCompletion(true);


        //Calculate y - f(x)

        //movieID = 4499;
        System.out.println(movieID);
        constants = new float[movieID];
        for (int i = 0; i < movieID; i++)
        {
            if ((i + 1) == targetID)
            {
                constants[i] = 1f;
            }
            else
            {
                constants[i] = (1 / movieID);
            }
        }
        System.out.println("Finished Grouping!");

        for (int i = 0; i < iteration; i++)
        {
            SaveConstants();
            System.out.println("Beginning on Linear Regression..");

            job = Job.getInstance(conf, "Regression");

            job.setJarByClass(Netflix.class);
            job.setMapperClass(RegressionMapper.class);
            job.setCombinerClass(RegressionReducer.class);
            job.setReducerClass(RegressionReducer.class);
            job.setMapOutputKeyClass(Text.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);
            FileInputFormat.addInputPath(job, new Path("NetflixOutput"));

            if (hdfs.exists(new Path("NetflixROutput")))
            {
                hdfs.delete(new Path("NetflixROutput"), true);
            }

            FileOutputFormat.setOutputPath(job, new Path("NetflixROutput"));

            job.waitForCompletion(true);

            System.out.println("Finished Regressing! Beginning on Error Calculation..");

            //userCount = 470774;

            rms = 0;

            job = Job.getInstance(conf, "Error Calculation");

            job.setJarByClass(Netflix.class);
            job.setMapperClass(ErrorMapper.class);
            job.setCombinerClass(ErrorReducer.class);
            job.setReducerClass(ErrorReducer.class);
            job.setMapOutputKeyClass(IntWritable.class);
            job.setMapOutputValueClass(Text.class);
            job.setOutputKeyClass(IntWritable.class);
            job.setOutputValueClass(Text.class);
            FileInputFormat.addInputPath(job, new Path("NetflixROutput"));

            if (hdfs.exists(new Path("NetflixEOutput")))
            {
                hdfs.delete(new Path("NetflixEOutput"), true);
            }

            FileOutputFormat.setOutputPath(job, new Path("NetflixEOutput"));

            job.waitForCompletion(true);

            SaveError();
        }*/

        movieID = 4499;
        userCount = 470774;

        relationships = new float[movieID][movieID];

        //Matchmake Relationships
        /*
        for (int i = 0; i < movieID; i++)
        {
            for (int j = 0; j < movieID; j++)
            {
                relationships[i][j] = 0;
            }
        }

        job = Job.getInstance(conf, "Relationship Mapping");

        job.setJarByClass(Netflix.class);
        job.setMapperClass(RelationshipMapper.class);
        job.setCombinerClass(RelationshipReducer.class);
        job.setReducerClass(RelationshipReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("NetflixOutput"));

        if (hdfs.exists(new Path("NetflixRelationship")))
        {
            hdfs.delete(new Path("NetflixRelationship"), true);
        }

        FileOutputFormat.setOutputPath(job, new Path("NetflixRelationship"));

        job.waitForCompletion(true);

        SaveRelationships();*/

        LoadRelationships();

        constants = new float[movieID];
        LoadConstants();

        predictions = new float[movieID];
        idArr = new int[movieID];

        job = Job.getInstance(conf, "Prediction");

        job.setJarByClass(Netflix.class);
        job.setMapperClass(RatingMapper.class);
        job.setCombinerClass(RatingReducer.class);
        job.setReducerClass(RatingReducer.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("PredictMe"));

        if (hdfs.exists(new Path("NetflixRatings")))
        {
            hdfs.delete(new Path("NetflixRatings"), true);
        }

        FileOutputFormat.setOutputPath(job, new Path("NetflixRatings"));

        job.waitForCompletion(true);
    }
}