import com.sun.org.apache.xpath.internal.operations.Mult;
import org.apache.avro.generic.GenericData;
import org.apache.commons.math3.analysis.function.Multiply;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class MarkovChain
{
    private static List<String> content;
    private static List<Node> nodes = new ArrayList<Node>();
    private static Hashtable<String, Node> dictionary = new Hashtable<String, Node>();
    private static Hashtable<String, Node> wide_dictionary = new Hashtable<>();
    private static Hashtable<String, Node> session_dictionary = new Hashtable<>();
    private static int depth;
    private static List<Float> constants = new ArrayList<Float>();
    private static List<Float> learning_values = new ArrayList<>();
    private static float learning_rate = 0.1f;
    private static float previous_error = 100;
    public static class SessionActionMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",");
            String ref, city;

            if (data[4].equals("search for destination"))
            {
                ref = data[5] + data[6];
                city = data[8] + data[9];
            }
            else
            {
                ref = data[5];
                city = data[7] + data[8];
            }

            context.write(new Text(data[1] + ";" + city), new Text(data[3] + "," + data[4] + " [" + ref + "]->"));
        }
    }

    public static class SessionActionReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static String FindNodeRecursive(String source_id, Node node, List<String> visitedNodes, int count, int depth_threshold)
    {
        if (node.id.equals(source_id))
        {
            depth = count;
            return node.id;
        }
        else if (visitedNodes.indexOf(node.id) != -1 || count > depth_threshold)
        {
            return "none";
        }
        else
        {
            visitedNodes.add(node.id);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            if (node.items.get(i).id.equals(source_id))
            {
                depth = count;
                return node.items.get(i).id;
            }
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            String find = FindNodeRecursive(source_id, node.items.get(i), visitedNodes, count + 1, depth_threshold);
            if (find.equals(source_id))
            {
                return find;
            }
        }
        return "none";
    }

    public static void MapNodeRecursive(Node node, List<String> visitedNodes, Node result, Node temp_result, int count, int depth_threshold, float preceding_probability, float constant, int const_index, String source_id, List<Float> x_values, List<Node> node_group)
    {
        if (visitedNodes.indexOf(node.id) != -1 || count > depth_threshold)
        {
            return;
        }
        else
        {
            visitedNodes.add(node.id);
        }

        Node temp = new Node("none");

        for (int i = 0; i < node.items.size(); i++)
        {
            float probability = preceding_probability * node.probability.get(i) * constant;
            result.Register(null, node.items.get(i).id, "", probability);
            //temp_result.Register(null, node.items.get(i).id, "", probability);

            node_group.get(count).Register(null, node.items.get(i).id, "", probability);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            //MapNodeRecursive(node.items.get(i), visitedNodes, result, temp_result, count + 1, depth_threshold, node.probability.get(i), constant); //Markov Chain Probability
            if (!(count + 1 > depth_threshold))
            {
                MapNodeRecursive(node.items.get(i), visitedNodes, result, temp_result, count + 1, depth_threshold, preceding_probability, constants.get(const_index + 1), const_index + 1, source_id, x_values, node_group); //Static Probability
            }
        }
    }

    public static void CalculateAccuracy(String sourceFile, String predictionFile, String resultFile) throws IOException
    {
        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        BufferedReader prediction = new BufferedReader(new FileReader(predictionFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter testWriter = new FileWriter("testWriter.txt");
        BufferedWriter testBuffer = new BufferedWriter(testWriter);

        String source_line, prediction_line;
        String source_id, prediction_id, region_id, session_id;
        String[] source_data, prediction_data;
        String nodeData;
        int left_bracket, right_bracket;
        float count = 0, correct = 0, none = 0, previous = 0, incorrect = 0, average_index = 0, average_depth = 0;
        List<String> visitedNodes = new ArrayList<>();
        List<String> checkedNodes = new ArrayList<>();
        List<String> addedNodes = new ArrayList<>();
        List<Float> x_values = new ArrayList<Float>();
        List<List<Float>> x_matrix = new ArrayList<>();
        List<List<Float>> y_values = new ArrayList<>();
        List<Float> session_length_list = new ArrayList<>();
        int session_action_length;
        float min_x_value = 100;

        float error = 0;
        int regression_index = 0;

        learning_values.clear();
        while ((source_line = source.readLine()) != null)
        {
            source_data = source_line.split("\\|");
            session_id = source_data[0].split(";")[0];
            region_id = source_data[0].split(";")[1];
            source_data = source_data[1].split("->");
            source_id = source_data[source_data.length - 1];
            left_bracket = source_id.indexOf("[");
            right_bracket = source_id.indexOf("]");
            source_id = "[" + source_id.substring(left_bracket + 1, right_bracket) + "]";

            int answer_index = -1;
            Node temp = new Node("none");
            prediction_id = "none";

            boolean found = false;
            nodeData = "none";
            depth = 0;
            Node result = new Node(session_id);
            Node temp_result = new Node("none");

            //Read from Previous Nodes, file 12 - 13
            visitedNodes.clear();
            checkedNodes.clear();
            addedNodes.clear();
            x_values.clear();
            if (source_data.length < 2)
            {
                List<Node> node_group = new ArrayList<>();
                node_group.add(new Node("none"));
                node_group.add(new Node("none"));
                if (constants.size() <= 0)
                {
                    constants.add(1.0f);
                    constants.add(1.0f);
                }
                source_data = source_line.split("\\|");
                source_data = source_data[0].split(";");
                nodeData = source_data[1];

                temp = dictionary.get(nodeData);
                if (temp != null)
                {
                    MapNodeRecursive(temp, checkedNodes, result, temp_result, 0, 1, 1.0f, constants.get(1), 0, source_id, x_values, node_group);
                    /*for (int j = 0; j < temp.items.size(); j++)
                    {
                        //System.out.println(temp.items.get(j).id);
                        //prediction_id = FindNodeRecursive(source_id, temp.items.get(j), visitedNodes, 0, 0); //BFS search

                        //System.out.println("Border");
                        if (prediction_id.equals(source_id))
                        {
                            answer_index = 0;
                            break;
                        }
                    }*/
                }

                //x_values start
                /*int temp_index = -1;
                for (int j = 0; j < temp_result.items.size(); j++)
                {
                    if (temp_result.items.get(j).id.equals(source_id))
                    {
                        temp_index = j;
                        break;
                    }
                }

                if (temp_index > -1)
                {
                    x_values.add(temp_result.count.get(temp_index));
                }
                else
                {
                    x_values.add(0.0f);
                }*/
                //x_values end

                for (int j = 0; j < node_group.size(); j++)
                {
                    int temp_index = -1;
                    for (int k = 0; k < node_group.get(j).items.size(); k++)
                    {
                        if (node_group.get(j).items.get(k).id.equals(source_id))
                        {
                            temp_index = k;
                            break;
                        }
                    }
                    if (temp_index > -1)
                    {
                        x_values.add(node_group.get(j).count.get(temp_index));
                    }
                    else
                    {
                        x_values.add(0f);
                    }
                }
            }
            else
            {
                for (int i = source_data.length - 2, const_index = 0; i >= 0 && !found; i--, const_index += 2)
                {
                    List<Node> node_group = new ArrayList<>();
                    node_group.add(new Node("none"));
                    node_group.add(new Node("none"));
                    if (constants.size() <= const_index)
                    {
                        constants.add(1.0f);
                        constants.add(1.0f);
                    }
                    nodeData = source_data[i];
                    left_bracket = nodeData.indexOf("[");
                    right_bracket = nodeData.indexOf("]");
                    String relationship = nodeData.substring(0, left_bracket - 1).split(",")[1];
                    String item = nodeData.substring(left_bracket + 1, right_bracket);
                    if (item.matches(".*[a-zA-Z]+.*"))
                    {
                        nodeData = "[" + relationship + "," + item + "," + region_id + "]";
                    }
                    else
                    {
                        nodeData = "[" + item + "]";
                    }

                    /*if(addedNodes.indexOf(nodeData) != -1)
                    {
                        continue;
                    }*/

                    temp = dictionary.get(nodeData);
                    temp_result = new Node(nodeData);
                    if (temp != null)
                    {
                        MapNodeRecursive(temp, checkedNodes, result, temp_result, 0, 1, 1.0f, constants.get(const_index), const_index, source_id, x_values, node_group);
                        /*for (int j = 0; j < temp.items.size(); j++)
                        {
                            //System.out.println(temp.items.get(j).id);
                            //prediction_id = FindNodeRecursive(source_id, temp.items.get(j), visitedNodes, 0, 0); //BFS search

                            //System.out.println("Border");
                            if (prediction_id.equals(source_id))
                            {
                                found = true;
                                break;
                            }
                        }*/
                    }

                    //x_values start
                    /*int temp_index = -1;
                    for (int j = 0; j < temp_result.items.size(); j++)
                    {
                        if (temp_result.items.get(j).id.equals(source_id))
                        {
                            temp_index = j;
                            break;
                        }
                    }

                    if (temp_index > -1)
                    {
                        x_values.add(temp_result.count.get(temp_index));
                    }
                    else
                    {
                        x_values.add(0.0f);
                    }*/
                    //x_values end

                    /*if (found)
                    {
                        answer_index = i;
                    }*/

                    //addedNodes.add(nodeData);

                    for (int j = 0; j < node_group.size(); j++)
                    {
                        int temp_index = -1;
                        for (int k = 0; k < node_group.get(j).items.size(); k++)
                        {
                            if (node_group.get(j).items.get(k).id.equals(source_id))
                            {
                                temp_index = k;
                                break;
                            }
                        }
                        if (temp_index > -1)
                        {
                            x_values.add(node_group.get(j).count.get(temp_index));
                        }
                        else
                        {
                            x_values.add(0f);
                        }
                    }
                }
            }

            /*if (prediction_id.equals("none"))
            {
                prediction_id = nodeData;
                //Region Check - 52
                if (!prediction_id.equals(source_id))
                {
                    temp = wide_dictionary.get(region_id);
                    if (temp != null)
                    {
                        for (int i = 0; i < temp.items.size(); i++)
                        {
                            if (temp.items.get(i).id.equals(source_id))
                            {
                                prediction_id = temp.items.get(i).id;
                                break;
                            }
                        }
                    }
                }
                //Bold Cosine Similarity - 53
                if (!prediction_id.equals(source_id))
                {
                    found = false;
                    source_data = source_line.split("\\|");
                    source_data = source_data[1].split("->");
                    for (int i = 0; i < source_data.length - 1 && !found; i++)
                    {
                        nodeData = source_data[i];
                        left_bracket = nodeData.indexOf("[");
                        right_bracket = nodeData.indexOf("]");
                        String relationship = nodeData.substring(0, left_bracket - 1).split(",")[1];
                        String item = nodeData.substring(left_bracket + 1, right_bracket);
                        if (item.matches(".*[a-zA-Z]+.*"))
                        {
                            nodeData = "[" + relationship + "," + item + "," + region_id + "]";
                        }
                        else
                        {
                            nodeData = "[" + item + "]";
                        }
                        //System.out.println(nodeData);
                        temp = session_dictionary.get(nodeData);
                        if (temp != null)
                        {
                            //System.out.println(temp.items.size());
                            for (int j = 0; j < temp.items.size(); j++)
                            {
                                if (temp.items.get(j).id.equals(source_id))
                                {
                                    prediction_id = temp.items.get(j).id;
                                    found = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            //System.out.println("It's a nah");
                        }
                    }
                }
                answer_index = (prediction_id.equals(source_id)) ? source_data.length - 1 : -1;
            }*/

            //Read from Prediction, file 1 - 11
            /*prediction_line = prediction.readLine();
            prediction_data = prediction_line.split("->");
            prediction_data = prediction_data[1].split("\\|");

            for (int i = 0; i < prediction_data.length; i++)
            {
                prediction_id = prediction_data[i];
                left_bracket = prediction_id.indexOf("[");
                right_bracket = prediction_id.indexOf("]");
                prediction_id = "[" + prediction_id.substring(left_bracket + 1, right_bracket) + "]";
                if (source_id.equals(prediction_id))
                {
                    answer_index = (i + 1);
                    break;
                }
            }*/

            result.CalculateProbability();
            result.SortProbability();

            float base = 0;
            for (int i = 0; i < result.items.size(); i++)
            {
                base += result.count.get(i);
            }

            float x_total = 0;
            for (int i = 0; i < x_values.size(); i++)
            {
                x_total += x_values.get(i);
            }

            for (int i = 0; i < result.items.size(); i++)
            {
                if (result.items.get(i).id.equals(source_id))
                {
                    System.out.println("Probability from Node : " + (result.probability.get(i) + 1.0f / base));
                    System.out.println("Manual Probability : " + x_total / base);
                    prediction_id = result.items.get(i).id;
                    answer_index = i;
                    break;
                }
            }

            /*if (prediction_id.equals("none"))
            {
                prediction_id = nodeData;
                answer_index = (prediction_id.equals(source_id)) ? source_data.length - 1 : -1;
            }*/

            if (answer_index != -1)
            {
                /*correct += (float)(1.0f / (float)answer_index);
                float remaining = (1.0f - (float)(1.0f / (float)answer_index));
                if (prediction_id.equals("none"))
                {
                    none += remaining;
                }
                else if (prediction_id.equals("previous"))
                {
                    previous += remaining;
                }
                else
                {
                    incorrect += remaining;
                }*/

                //correct++;
                correct += (float)(1.0f / (float)(answer_index + 1));
                float remaining = (1.0f - (float)(1.0f / (float)(answer_index + 1)));
                incorrect += remaining;
            }
            else
            {
                depth = 0;
                answer_index = source_data.length - 1;
                if (prediction_id.equals("none"))
                {
                    none++;
                }
                else if (prediction_id.equals("previous"))
                {
                    previous++;
                }
                else
                {
                    incorrect++;
                }
            }

            float session_length = source_data.length - 1;
            buffer.write(source_id + ";" + prediction_id + ";" + nodeData + ";" + answer_index + ";" + session_length  + ";" + depth);
            buffer.newLine();

            average_depth += depth;
            average_index += ((session_length > 0) ? (float)answer_index / session_length : 1.0);

            System.out.println(count);
            count++;

            /*testBuffer.write(result.id);
            testBuffer.newLine();
            for (int i = 0; i < result.items.size() && i < 25; i++)
            {
                testBuffer.write(result.items.get(i).id + ";" + result.probability.get(i));
                testBuffer.newLine();
            }*/

            if (x_total > 0.0f)
            {
                if (result.items.size() > 0)
                {
                    //Error function
                    float y = result.probability.get(0) * 1.1f;
                    error += Math.pow(y - (x_total / base), 2.0);

                    //Learning function
                    float base_error = y - (x_total / base);
                    for (int i = 0; i < x_values.size(); i++)
                    {
                        if (learning_values.size() <= i)
                        {
                            learning_values.add(0.0f);
                        }
                        learning_values.set(i, learning_values.get(i) + base_error * (x_values.get(i) / base));
                    }

                    for (int i = 0; i < x_values.size(); i++)
                    {
                        x_values.set(i, x_values.get(i) / base);
                        if (x_values.get(i) < min_x_value)
                        {
                            min_x_value = x_values.get(i);
                        }
                    }

                    x_matrix.add(new ArrayList<>());
                    for (int i = 0; i < x_values.size(); i++)
                    {
                        x_matrix.get(x_matrix.size() - 1).add(x_values.get(i));
                    }
                    y_values.add(new ArrayList<>());
                    y_values.get(y_values.size() - 1).add(y);
                    session_length_list.add((float)x_values.size());
                    regression_index++;
                }
            }
        }
        error /= (2.0f * regression_index);
        System.out.println("Learning values size : " + learning_values.size());
        System.out.println("Constants size : " + constants.size());
        for (int i = 0; i < learning_values.size(); i++)
        {
            learning_values.set(i, learning_values.get(i) * learning_rate / regression_index);
            constants.set(i, constants.get(i) + learning_values.get(i));
        }
        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Previous : " + (previous / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();
        buffer.write("Average Depth : " + (average_depth / count));
        buffer.newLine();
        buffer.write("Average Length : " + (average_index * 100 / count));
        buffer.newLine();
        buffer.write("Error : " + error);
        buffer.newLine();
        buffer.write("Constants : ");
        buffer.newLine();
        for (int i = 0; i < constants.size(); i++)
        {
            buffer.write(constants.get(i) + ";");
        }
        buffer.newLine();

        testBuffer.close();

        //Learning rate incremental start
        /*if (error < previous_error)
        {
            learning_rate *= 1.05f;
        }
        else
        {
            learning_rate *= 0.5f;
        }
        previous_error = error;*/
        //Learning rate incremental end

        //Inverse method start
        float mean_session_length = 0;
        for (int i = 0; i < session_length_list.size(); i++)
        {
            mean_session_length += session_length_list.get(i);
        }
        mean_session_length /= regression_index;

        for (int i = 0; i < x_matrix.size(); i++)
        {
            while (x_matrix.get(i).size() < mean_session_length)
            {
                x_matrix.get(i).add(0.0f);
            }
            while (x_matrix.get(i).size() > mean_session_length)
            {
                x_matrix.get(i).remove(x_matrix.get(i).size() - 1);
            }
        }

        min_x_value /= mean_session_length;
        for (int i = 0; i < x_matrix.size(); i++)
        {
            for (int j = 0; j < x_matrix.get(i).size(); j++)
            {
                x_matrix.get(i).set(j, x_matrix.get(i).get(j) + min_x_value);
            }
        }

        min_x_value *= mean_session_length;
        for (int i = 0; i < y_values.size(); i++)
        {
            for (int j = 0; j < y_values.get(i).size(); j++)
            {
                y_values.get(i).set(j, y_values.get(i).get(j) + min_x_value);
            }
        }

        List<List<Float>> transpose = TransposeMatrix(x_matrix);
        List<List<Float>> interim = MultiplyMatrix(transpose, x_matrix);
        List<List<Float>> interim_inv = InverseMatrix(interim);
        List<List<Float>> interim_2 = MultiplyMatrix(interim_inv, transpose);
        List<List<Float>> result = MultiplyMatrix(interim_2, y_values);

        SaveMatrix(x_matrix, "Matrix : ", buffer);
        SaveMatrix(transpose, "Transpose : ", buffer);
        SaveMatrix(interim, "Interim : ", buffer);
        SaveMatrix(interim_inv, "Interim (Inv) : ", buffer);
        SaveMatrix(interim_2, "Interim 2 : ", buffer);

        buffer.write("Constants (Inv) : ");
        buffer.newLine();
        for (int i = 0; i < result.size(); i++)
        {
            for (int j = 0; j < result.get(i).size(); j++)
            {
                buffer.write(result.get(i).get(j) + ";");
            }
        }

        List<List<Float>> inverse_check = MultiplyMatrix(interim, interim_inv);
        SaveMatrix(inverse_check, "Just To Check If Inverse Works : ", buffer);
        //Inverse method end

        buffer.newLine();
        buffer.close();
    }

    static public void SaveMatrix(List<List<Float>> matrix, String name, BufferedWriter buffer) throws IOException
    {
        buffer.write(name);
        buffer.newLine();
        /*for (int i = 0; i < matrix.size(); i++)
        {
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                buffer.write(matrix.get(i).get(j) + ";");
            }
            buffer.newLine();
        }*/
        buffer.write(matrix.size() + ";" + matrix.get(0).size());
        buffer.newLine();
    }

    static public List<List<Float>> TransposeMatrix(List<List<Float>> matrix)
    {
        List<List<Float>> temp = new ArrayList<>();
        for (int i = 0; i < matrix.get(0).size(); i++)
        {
            temp.add(new ArrayList<>());
            for (int j = 0; j < matrix.size(); j++)
            {
                temp.get(i).add(matrix.get(j).get(i));
            }
        }
        return temp;
    }

    static public List<List<Float>> MultiplyMatrix(List<List<Float>> left, List<List<Float>> right)
    {
        List<List<Float>> result = new ArrayList<>();
        for (int i = 0; i < left.size(); i++)
        {
            result.add(new ArrayList<>());
            for (int j = 0; j < right.get(0).size(); j++)
            {
                float total = 0;
                for (int k = 0; k < left.get(i).size(); k++)
                {
                    total += left.get(i).get(k) * right.get(k).get(j);
                }
                result.get(i).add(total);
            }
        }
        return result;
    }

    static public List<List<Float>> InverseMatrix(List<List<Float>> matrix)
    {
        List<List<Float>> temp = new ArrayList<>();
        List<List<Float>> result = new ArrayList<>();
        if (matrix.size() != matrix.get(0).size())
        {
            System.out.println("Unable to inverse the matrix");
            return result;
        }

        //Identity Matrix
        for (int i = 0; i < matrix.size(); i++)
        {
            result.add(new ArrayList<>());
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                if (i == j)
                {
                    result.get(i).add(1.0f);
                }
                else
                {
                    result.get(i).add(0f);
                }
            }
        }

        //Temp Matrix
        for (int i = 0; i < matrix.size(); i++)
        {
            temp.add(new ArrayList<>());
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                temp.get(i).add(matrix.get(i).get(j));
            }
        }

        //Zero Triangle
        for (int i = 1; i < temp.size(); i++)
        {
            for (int j = i; j < temp.get(i).size(); j++)
            {
                float multiplier;
                if (temp.get(j).get(i-1) == 0)
                {
                    multiplier = 0;
                }
                else
                {
                    multiplier = (temp.get(j).get(i-1) / temp.get(i-1).get(i-1));
                }
                for (int k = 0; k < temp.get(i).size(); k++)
                {
                    temp.get(j).set(k, temp.get(j).get(k) - temp.get(i-1).get(k) * multiplier);
                    result.get(j).set(k, result.get(j).get(k) - result.get(i-1).get(k) * multiplier);
                }
            }
        }

        //The Remaining Zero(s)
        for (int i = 0; i < temp.size() - 1; i++)
        {
            for (int j = i + 1; j < temp.get(i).size(); j++)
            {
                float multiplier;
                if (temp.get(i).get(j) == 0)
                {
                    multiplier = 0;
                }
                else
                {
                    multiplier = (temp.get(i).get(j) / temp.get(j).get(j));
                }
                for (int k = 0; k < temp.get(i).size(); k++)
                {
                    temp.get(i).set(k, temp.get(i).get(k) - temp.get(j).get(k) * multiplier);
                    result.get(i).set(k, result.get(i).get(k) - result.get(j).get(k) * multiplier);
                }
            }
        }

        //Simplification
        for (int i = 0; i < temp.size(); i++)
        {
            float multiplier = temp.get(i).get(i);
            for (int j = 0; j < temp.get(i).size(); j++)
            {
                result.get(i).set(j, result.get(i).get(j) / multiplier);
            }
        }

        return result;
    }

    public static void RecalculateAccuracy(String sourceFile, String resultFile) throws IOException
    {
        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        String source_line;
        String[] source_data;
        float count = 0, correct = 0, none = 0, previous = 0, incorrect = 0;
        while ((source_line = source.readLine()) != null)
        {
            if (source_line.contains(";"))
            {
                source_data = source_line.split(";");
                if (source_data[0].equals(source_data[1]))
                {
                    System.out.println(source_data[0] + " " + source_data[1]);
                    correct++;
                }
                else
                {
                    if (source_data[1].equals("none"))
                    {
                        none++;
                    }
                    else
                    {
                        incorrect++;
                    }
                }
                buffer.write(source_line);
                buffer.newLine();
                count++;
            }
        }
        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Previous : " + (previous / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();
        buffer.close();
    }

    public static int FindRegion(String region)
    {
        for (int i = 0; i < nodes.size(); i++)
        {
            if (nodes.get(i).id.equals(region))
            {
                return i;
            }
        }
        return -1;
    }

    public static void ItemTreeMap(String inFile, String outFile) throws IOException
    {
        nodes.clear();
        dictionary.clear();
        wide_dictionary.clear();
        session_dictionary.clear();
        String[] data, session;
        String regionID;
        int regionIndex;
        String line;
        Node temp, existing;
        List<String> item_list = new ArrayList<String>();
        int count = 0;

        System.out.println("Mapping graph..");
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        while ((line = reader.readLine()) != null)
        {
            data = line.split(";");
            data = data[1].split("\\|");

            regionID = data[0];
            regionIndex = FindRegion(regionID);

            if (regionIndex == -1)
            {
                nodes.add(new Node(regionID));
                regionIndex = nodes.size() - 1;
                dictionary.put(regionID, nodes.get(regionIndex));
                wide_dictionary.put(regionID, new Node(regionID));
            }

            data = data[1].split("->");
            temp = nodes.get(regionIndex);
            item_list.clear();
            for (int i = 0; i < data.length; i++)
            {
                session = data[i].split(",");
                int left_bracket = session[1].indexOf("[");
                int right_bracket = session[1].indexOf("]");
                String relationship = session[1].substring(0, left_bracket - 1);
                String item = session[1].substring(left_bracket + 1, right_bracket);
                String item_combined;
                if (item.matches(".*[a-zA-Z]+.*"))
                {
                    item_combined = "[" + relationship + "," + item + "," + regionID + "]";
                }
                else
                {
                    item_combined = "[" + item + "]";
                }

                if (!(item.equals(regionID) && relationship.equals("search for destination")))
                {
                    item_list.add(item_combined);
                    existing = dictionary.get(item_combined);
                    if (existing == null)
                    {
                        existing = new Node(item_combined);
                        dictionary.put(item_combined, existing);
                    }
                    temp = temp.Register(existing, item_combined, "", 1f);
                    wide_dictionary.get(regionID).Register(existing, item_combined, "", 1f);
                }
            }

            //Cosine Similarity - 53
            /*for (int i = 0; i < item_list.size(); i++)
            {
                Node base = session_dictionary.get(item_list.get(i));
                if (base == null)
                {
                    session_dictionary.put(item_list.get(i), new Node(item_list.get(i)));
                    base = session_dictionary.get(item_list.get(i));
                }
                for (int j = 0; j < item_list.size(); j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    existing = session_dictionary.get(item_list.get(j));
                    if (existing == null)
                    {
                        existing = new Node(item_list.get(j));
                    }
                    base.Register(existing, item_list.get(j), "");
                }
            }*/
            count++;
            System.out.println(count);
        }
        reader.close();

        System.out.println("Saving graph to file..");
        SaveGraphToFile(outFile);
    }

    public static void LoadFile(String fileName) throws IOException {
        content = new ArrayList<String>();
        String line;
        int count = 0;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null) {
            content.add(line);
            count++;
        }
        reader.close();
    }

    public static void SaveGraphToFile(String fileName) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);
        Set<String> keys = dictionary.keySet();

        Node node;
        float total, average, stdDev;
        for(String key: keys)
        {
            node = dictionary.get(key);
            total = 0;
            average = 0;
            stdDev = 0;

            node.CalculateProbability();
            node.SortProbability();
            for (int i = 0; i < node.items.size(); i++)
            {
                total += node.probability.get(i);
            }
            average = total / (float)node.items.size();
            for (int i = 0; i < node.items.size(); i++)
            {
                stdDev += Math.pow((node.probability.get(i) - average), 2.0);
            }
            stdDev /= node.items.size();
            stdDev = (float)Math.pow(stdDev, 0.5);

            buffer.write("#" + node.id + "#");
            buffer.newLine();
            buffer.write("Total : " + total);
            buffer.newLine();
            buffer.write("Average + Std Dev : " + (average + stdDev));
            buffer.newLine();
            for (int i = 0; i < node.items.size(); i++)
            {
                buffer.write(node.items.get(i).id + ";" + node.probability.get(i));
                buffer.newLine();
            }
        }
        buffer.close();
    }

    public static void SessionPredict(String inFile, String outFile, int depth) throws IOException
    {
        String line, regionID;
        String[] data, session;
        int regionIndex;
        Node temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        /*while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            regionID = data[0].split(";")[1];
            regionIndex = FindRegion(regionID);

            if (regionIndex == -1)
            {
                buffer.write("none" + "->" + "none[none];0");
                buffer.newLine();
                continue;
            }

            if (data.length < 2)
            {
                temp = dictionary.get(regionID);
                String answer;
                if (temp.items.size() > 0)
                {
                    answer = temp.items.get(0).id + ";" + temp.probability.get(0);
                }
                else
                {
                    answer = "previous[previous];0";
                }
                buffer.write( temp.id + "->" + answer);
                buffer.newLine();
                continue;
            }

            data = data[1].split("->");
            for (int i = data.length - 1, j = 0; i >= 0 && j < depth; i--, j++)
            {
                session = data[i].split(",");
                int left_bracket = session[1].indexOf("[");
                int right_bracket = session[1].indexOf("]");
                String relationship = session[1].substring(0, left_bracket - 1);
                String item = session[1].substring(left_bracket + 1, right_bracket);
                String item_combined = "[" + item + "]";

                temp = dictionary.get(item_combined);

                if (temp != null)
                {
                    if (temp.items.size() > 0)
                    {
                        //buffer.write(item_combined + "->" + temp.items.get(0).id + ";" + temp.probability.get(0));
                        buffer.write(item_combined + "->");
                        for (int k = 0; k < temp.items.size(); k++)
                        {
                            buffer.write(temp.items.get(k).id + ";" + temp.probability.get(k) + "|");
                        }
                    }
                    else
                    {
                        buffer.write(item_combined + "->" + item_combined + ";1.0");
                    }
                }
                else
                {
                    buffer.write(item_combined + "->" + item_combined + ";1.0");
                }
            }
            buffer.newLine();
        }*/
        buffer.close();
    }

    public static void SessionSort(String fileName, String fileName2, int boundaryTop, int boundaryBottom) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter writer2 = new FileWriter(fileName2);
        BufferedWriter buffer2 = new BufferedWriter(writer2);

        String[] arr, point, bites;
        List<String> sessions = new ArrayList<String>();
        List<Integer> index = new ArrayList<Integer>();
        int tempInt, step;
        String tempStr, newContent, lastSeen;

        for (int i = 0; i < content.size(); i++)
        {
            arr = content.get(i).split("\\|");
            sessions.clear();
            index.clear();

            point = arr[1].split("->");
            boolean proceed = true;
            for (int j = 0; j < point.length; j++) {
                if (!point[j].equals("")) {
                    bites = point[j].split(",");
                    try {
                        index.add(Integer.parseInt(bites[0]));
                        sessions.add(bites[1]);
                    } catch (Exception e) {
                        proceed = false;
                        break;
                    }
                }
            }

            if (!proceed) {
                continue;
            }

            for (int j = 0; j < index.size(); j++)
            {
                for (int k = j; k < index.size(); k++)
                {
                    if (index.get(k) < index.get(j))
                    {
                        tempInt = index.get(j);
                        tempStr = sessions.get(j);

                        index.set(j, index.get(k));
                        index.set(k, tempInt);

                        sessions.set(j, sessions.get(k));
                        sessions.set(k, tempStr);
                    }
                }
            }

            newContent = "";
            lastSeen = "";
            step = 1;

            for (int j = 0; j < index.size(); j++)
            {
                if (!sessions.get(j).equals(lastSeen))
                {
                    newContent += step + "," + sessions.get(j);
                    newContent += "->";
                    step++;
                    lastSeen = sessions.get(j);
                }
            }
            newContent = newContent.substring(0, newContent.length() - 2);

            //content.set(i, newContent);
            if (i >= boundaryTop && i < boundaryBottom)
            {
                buffer2.write(arr[0] + "|" + newContent);
                buffer2.newLine();
            }
            else
            {
                buffer.write(arr[0] + "|" + newContent);
                buffer.newLine();
            }
        }
        buffer.close();
        buffer2.close();
    }

    public static void SessionTestFilter(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            data = data[1].split("->");
            for (int i = 0; i < data.length; i++)
            {
                if (data[i].contains("clickout"))
                {
                    temp = session + "|";
                    for (int j = 0; j <= i; j++)
                    {
                        temp += data[j] + "->";
                    }
                    temp = temp.substring(0, temp.length() - 2);
                    buffer.write(temp);
                    buffer.newLine();
                }
            }
        }
        buffer.close();
    }

    public static void SessionTestHide(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            temp = session + "|";
            data = data[1].split("->");
            for (int i = 0; i < data.length - 1; i++)
            {
                temp += data[i] + "->";
            }
            if (data.length > 1)
            {
                temp = temp.substring(0, temp.length() - 2);
            }
            buffer.write(temp);
            buffer.newLine();
        }
        buffer.close();
    }

    public static void main(String[] args) throws Exception
    {
        long start = System.currentTimeMillis();

        /*Configuration conf = new Configuration();
        conf.set(TextOutputFormat.SEPERATOR, "|");
        FileSystem hdfs = FileSystem.get(conf);
        Job job;

        job = Job.getInstance(conf, "Session Action List");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(SessionActionMapper.class);
        job.setCombinerClass(SessionActionReducer.class);
        job.setReducerClass(SessionActionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train"));
        if (hdfs.exists(new Path("trivago_train_session")))
        {
            hdfs.delete(new Path("trivago_train_session"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_session"));
        job.waitForCompletion(true);*/

        LoadFile("trivago_train_session/part-r-00000");

        float boundaryTop = 0.9f;
        float boundaryBottom = 1.0f;
        int fileIndex = 100;

        String session_train, session_test, session_probability, session_test_filtered;
        String session_test_filtered_hidden, session_prediction, session_accuracy;

        //Pre-computed constants start
        constants.add(1.1628036f);
        constants.add(1.364749f);
        constants.add(1.2368172f);
        constants.add(1.6841427f);
        constants.add(1.2589458f);
        constants.add(1.6549934f);
        constants.add(1.2637573f);
        constants.add(2.142731f);
        constants.add(1.31396f);
        constants.add(2.043183f);
        //Pre-computed constants end

        while (fileIndex < 101)
        {
            System.out.println("Iteration : " + fileIndex);

            session_train = "trivago_session/session_train" + fileIndex + ".txt";
            session_test = "trivago_session/session_test" + fileIndex + ".txt";
            session_probability = "trivago_session/probability" + fileIndex + ".txt";
            session_test_filtered = "trivago_session/session_test_filtered" + fileIndex + ".txt";
            session_test_filtered_hidden = "trivago_session/session_test_filtered_hidden" + fileIndex + ".txt";
            session_prediction = "trivago_session/prediction" + fileIndex + ".txt";
            session_accuracy = "trivago_session/accuracy" + fileIndex + ".txt";

            SessionSort(session_train, session_test, (int)(content.size() * boundaryTop), (int)(content.size() * boundaryBottom));
            ItemTreeMap(session_train, session_probability);
            SessionTestFilter(session_test, session_test_filtered);
            SessionTestHide(session_test_filtered, session_test_filtered_hidden);
            SessionPredict(session_test_filtered_hidden, session_prediction, 1);
            CalculateAccuracy(session_test_filtered, session_prediction, session_accuracy);
            //boundaryTop += 0.1f;
            //boundaryBottom += 0.1f;
            fileIndex++;
            //RecalculateAccuracy("trivago_session/accuracy" + (fileIndex - 1) + ".txt", "trivago_session/accuracy" + (fileIndex - 1) + "_recalculated.txt");
        }
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
    }
}
