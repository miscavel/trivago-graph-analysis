import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.apache.avro.generic.GenericData;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import javax.print.attribute.standard.Destination;

public class Trivago {
    static String last_user, last_session, last_destination;
    private static List<String> content, sequence, hotels;
    private static List<Node> nodes;

    public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        String line;

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            line = value.toString();
            String data[];

            data = line.split(",(?!\\s)");

            if (data[4].equals("clickout item")) {
                word.set(data[7] + "|" + data[5]);
                context.write(word, one);
            }
        }
    }

    public static class IntSumReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        IntWritable result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int sum = 0;
            for (IntWritable val : values) {
                sum += val.get();
            }
            result.set(sum);
            context.write(key, result);
        }
    }

    public static class DestinationMapper extends Mapper<Object, Text, Text, Text>
    {
        String line, user, session, destination;
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            line = value.toString();
            String data[];

            data = line.split(",(?!\\s)");

            user = data[0];
            session = data[1];
            destination = data[7];

            boolean write = true;

            if (user.equals(last_user))
            {
                if (session.equals(last_session))
                {
                    if (destination.equals(last_destination))
                    {
                        write = false;
                    }
                }
            }
            else if (user.equals("user_id"))
            {
                write = false;
            }

            if (write)
            {
                context.write(new Text(user), new Text(session + "!" + destination + ";"));
            }

            last_user = user;
            last_session = session;
            last_destination = destination;
        }
    }

    public static class DestinationReducer extends Reducer<Text, Text, Text, Text>
    {
        String combined;
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            combined = "";
            for (Text val : values)
            {
                combined += val.toString();
            }
            context.write(key, new Text(combined));
        }
    }

    public static class HotelMapper extends Mapper<Object, Text, Text, IntWritable> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        String line;

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            line = value.toString();
            String data[];

            data = line.split(",(?!\\s)");

            if (data[4].equals("clickout item")) {
                word.set(data[5]);
                context.write(word, one);
            }
        }
    }

    public static class HotelReducer extends Reducer<Text, IntWritable, Text, IntWritable> {
        IntWritable result = new IntWritable();

        public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
            int total = 0;
            int count = 0;
            for (IntWritable val : values) {
                total += val.get();
                count++;
            }

            result.set(total);
            context.write(key, result);
        }
    }

    public static class HotelClusterMapper extends Mapper<Object, Text, Text, Text> {
        private Text word = new Text();
        String line;

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            line = value.toString();
            String arr[], step[];
            String hotelIndex = "", clusterString = "";
            int stopIndex = -1, pointer = 0;
            boolean insert;

            arr = line.split("\\|");
            step = arr[1].split("->");
            System.out.println(arr[1]);
            System.out.println(step.length);
            while (pointer < step.length)
            {
                clusterString = "";
                insert = false;
                for (int k = pointer; k < step.length; k++, pointer = k)
                {
                    if (k != 0)
                    {
                        if (step[k].contains("clickout"))
                        {
                            insert = true;
                            stopIndex = k + 1;
                            hotelIndex = step[k].substring(step[k].indexOf("(") + 1, step[k].indexOf(")"));
                            pointer++;
                            break;
                        }
                    }
                }
                if (insert)
                {
                    for (int k = 0; k < stopIndex; k++)
                    {
                        clusterString += step[k] + "->";
                    }
                    clusterString = clusterString.substring(0, clusterString.length() - 2);
                    clusterString += ";";
                    context.write(new Text(hotelIndex), new Text(clusterString));
                }
            }
        }
    }

    public static class HotelClusterReducer extends Reducer<Text, Text, Text, Text> {
        Text result = new Text();

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values) {
                value += val.toString();
            }
            result.set(value);
            context.write(key, result);
        }
    }

    /*
    public static class ItemMapper extends Mapper<Object, Text, Text, Text> {
        private Text word = new Text();
        String line;

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String[] data, session;
            String regionID;
            int regionIndex, previousIndex = -1;

            line = value.toString();

            data = line.split(";");
            data = data[1].split("\\|");

            regionID = data[0];
            regionIndex = FindRegion(regionID);

            if (regionIndex == -1)
            {
                nodes.add(new Node(regionID));
                regionIndex = nodes.size() - 1;
            }

            data = data[1].split("->");
            for (int i = 0; i < data.length; i++)
            {
                session = data[i].split(",");
                int left_bracket = session[1].indexOf("(");
                int right_bracket = session[1].indexOf(")");
                String item = session[1].substring(0, left_bracket - 1);
                String relationship = session[1].substring(left_bracket, right_bracket);
                if (i == 0)
                {
                    previousIndex = nodes.get(regionIndex).Register(item, relationship);
                }
                else
                {
                    nodes.get(regionIndex).items.get(previousIndex).Register(item, relationship);
                }
            }
        }
    }

    public static class ItemReducer extends Reducer<Text, Text, Text, Text> {
        Text result = new Text();

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values) {
                value += val.toString();
            }
            result.set(value);
            context.write(key, result);
        }
    }
    */

    public static class ListMapper extends Mapper<Object, Text, Text, Text> {
        private Text word = new Text(), content = new Text();
        String line;

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            line = value.toString();
            String data[];

            data = line.split("\\|");
            word.set(data[0]);
            content.set(data[1] + "," + data[2]);
            context.write(word, content);
        }
    }

    public static class ListReducer extends Reducer<Text, Text, Text, Text> {
        Text result = new Text();

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            String value = "";
            for (Text val : values) {
                value += val.toString() + ";";
            }
            result.set(value.substring(0, value.length() - 1));
            context.write(key, result);
        }
    }

    public static class SessionMapper extends Mapper<Object, Text, Text, Text>
    {
        private Text word = new Text();
        String line;
        int add;

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            line = value.toString();
            String data[];
            String ref = "", city = "";

            data = line.split(",");

            if (data[4].equals("search for destination"))
            {
                ref = data[5] + data[6];
                city = data[8] + data[9];
            }
            else
            {
                ref = data[5];
                city = data[7] + data[8];
            }

            context.write(new Text(data[1] + ";" + city), new Text(data[3] + "," + data[4] + " [" + ref + "]->"));
        }
    }

    public static class SessionReducer extends Reducer<Text, Text, Text, Text> {
        Text result = new Text();

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            String value = "";
            int count = 0;
            for (Text val : values) {
                value += val.toString();
                count++;
            }
            /*if (count == 1 && value.indexOf("->") != value.length() - 2)
            {
                value = value.substring(0, value.length() - 4);
            }*/
            result.set(value);
            context.write(key, result);
        }
    }

    public static class UserMapper extends Mapper<Object, Text, Text, Text> {
        private final static IntWritable one = new IntWritable(1);
        private Text word = new Text();
        String line;

        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            line = value.toString();
            String data[];

            data = line.split(",(?!\\s)");

            if (data[4].equals("clickout item")) {
                word.set(data[0]);
                context.write(word, new Text(data[1]));
            }
        }
    }

    public static class UserReducer extends Reducer<Text, Text, Text, Text> {
        Text result = new Text();

        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
            String value = "";
            int count = 0;
            for (Text val : values) {
                value += val.toString() + "|";
                count++;
            }
            if (count == 1) {
                value = value.substring(0, value.length() - 2);
            }
            result.set(value);
            context.write(key, result);
        }
    }

    public static boolean CheckRecurrentDestination(ArrayList<ArrayList<String>> destinations)
    {
        for (int i = 0; i < destinations.size(); i++)
        {
            for (int j = 0; j < destinations.size(); j++)
            {
                if (i == j)
                {
                    break;
                }
                else
                {
                    for (int k = 0; k < destinations.get(i).size(); k++)
                    {
                        for (int l = 0; l < destinations.get(j).size(); l++)
                        {
                            if (destinations.get(i).get(k).equals(destinations.get(j).get(l)))
                            {
                                System.out.println(destinations.get(i).get(k) + " | " + destinations.get(j).get(l));
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void DestinationCullSession(String infile, String outfile, int min_session) throws IOException
    {
        int sessionCount;
        String line, currentSession;
        String data[], lump[];
        BufferedReader reader = new BufferedReader(new FileReader(infile));
        FileWriter writer = new FileWriter(outfile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            data = data[1].split(";");
            sessionCount = 0;
            currentSession = "";

            for (int i = 0; i < data.length; i++)
            {
                lump = data[i].split("!");
                if (!lump[0].equals(currentSession))
                {
                    currentSession = lump[0];
                    sessionCount++;
                }
            }

            if (sessionCount >= min_session)
            {
                buffer.write(line);
                buffer.newLine();
            }
        }
        reader.close();
        buffer.close();
    }

    public static void DestinationMultiSession(String infile, String outfile) throws IOException
    {
        ArrayList<ArrayList<String>> destinations = new ArrayList<ArrayList<String>>();
        ArrayList<String> destination = new ArrayList<String>();
        String line, currentSession;
        String data[], lump[];
        BufferedReader reader = new BufferedReader(new FileReader(infile));
        FileWriter writer = new FileWriter(outfile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            data = data[1].split(";");
            currentSession = "";

            destinations.clear();
            destination.clear();
            for (int i = 0; i < data.length; i++)
            {
                lump = data[i].split("!");
                if (!lump[0].equals(currentSession))
                {
                    currentSession = lump[0];
                    if(destination.size() > 0)
                    {
                        destinations.add(destination);
                    }
                    destination.clear();
                }
                destination.add(lump[1]);
            }

            if (CheckRecurrentDestination(destinations))
            {
                buffer.write(line);
                buffer.newLine();
            }
        }
        reader.close();
        buffer.close();
    }

    public static Node FindNode(Node node, String item, List<String> visitedNodes)
    {
        if (visitedNodes.contains(node.id))
        {
            return null;
        }
        for (int i = 0; i < node.items.size(); i++)
        {
            if (node.items.get(i).id.equals(item))
            {
                return node.items.get(i);
            }
            else
            {
                visitedNodes.add(node.items.get(i).id);
                Node temp = FindNode(node.items.get(i), item, visitedNodes);
                if (temp != null)
                {
                    return temp;
                }
            }
        }
        return null;
    }

    public static void FindMinMaxTimestamp(String fileName) throws IOException
    {
        String line;
        String data[];
        int min = -1, max = -1, timestamp;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null)
        {
            data = line.split(",");
            try
            {
                timestamp = Integer.parseInt(data[2]);
                if (min == -1)
                {
                    min = timestamp;
                }
                else
                {
                    min = (timestamp < min) ? timestamp : min;
                }

                if (max == -1)
                {
                    max = timestamp;
                }
                else
                {
                    max = (timestamp > max) ? timestamp : max;
                }
            }
            catch(Exception e)
            {

            }
        }
        reader.close();
        System.out.println("Min : " + min);
        System.out.println("Max : " + max);
    }

    public static int FindRegion(String region)
    {
        for (int i = 0; i < nodes.size(); i++)
        {
            if (nodes.get(i).id.equals(region))
            {
                return i;
            }
        }
        return -1;
    }

    public static void HotelClickoutBasedCluster(String fileName) throws IOException
    {
        String[] arr, step;
        String hotelIndex;
        String clusterString;
        int stopIndex;
        List<String> clusters = new ArrayList<String>();
        System.out.println("Clustering sequence on hotels");
        for (int i = 0; i < hotels.size(); i++)
        {
            arr = hotels.get(i).split("\\|");
            hotelIndex = arr[0];
            System.out.println(hotelIndex);
            clusterString = hotelIndex + ";";
            for (int j = 0; j < sequence.size(); j++)
            {
                arr = sequence.get(j).split("\\|");
                step = arr[1].split("->");
                stopIndex = -1;
                for (int k = 0; k < step.length; k++)
                {
                    if (k != 0)
                    {
                        if (step[k].contains("clickout"))
                        {
                            if (step[k].substring(step[k].indexOf("("), step[k].indexOf(")")).equals(hotelIndex))
                            {
                                stopIndex = k;
                            }
                        }
                    }
                }
                if (stopIndex != -1)
                {
                    clusterString += arr[0] + "|";
                    for (int k = 0; k < stopIndex; k++)
                    {
                        clusterString += step[k] + "->";
                    }
                    clusterString = clusterString.substring(0, clusterString.length() - 2);
                    clusterString += ";";
                }
            }
            clusters.add(clusterString);
        }
        System.out.println("Hotel clustering done!");

        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);

        System.out.println("Writing to file..");
        for (int i = 0; i < clusters.size(); i++)
        {
            arr = clusters.get(i).split(";");
            buffer.write(arr[0]);
            buffer.newLine();
            for (int j = 1; j < arr.length; j++)
            {
                buffer.write(arr[j]);
                buffer.newLine();
            }
            buffer.newLine();
        }
        buffer.close();
        System.out.println("Writing done!");
    }

    public static void HotelSort(String fileName) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);
        int temp;
        List<Integer> hotels = new ArrayList<Integer>();
        String[] arr;
        System.out.println("Begin..");
        for (int i = 0; i < content.size(); i++) {
            arr = content.get(i).split("\\|");
            System.out.println(arr[0]);
            hotels.add(Integer.parseInt(arr[0]));
        }

        for (int i = 0; i < hotels.size(); i++) {
            System.out.println("Sorting..");
            for (int j = i; j < hotels.size(); j++) {
                if (hotels.get(j) < hotels.get(i)) {
                    temp = hotels.get(i);
                    hotels.set(i, hotels.get(j));
                    hotels.set(j, temp);
                }
            }
        }

        for (int i = 0; i < hotels.size(); i++) {
            buffer.write(hotels.get(i));
            buffer.newLine();
        }
        buffer.close();
        System.out.println("End");
    }

    public static void ItemRecursiveMap(BufferedWriter buffer, Node node, List<String> visitedNodes) throws IOException
    {
        if (visitedNodes.indexOf(node.id) != -1)
        {
            return;
        }
        float total = 0, average, stdDev = 0;
        node.CalculateProbability();
        node.SortProbability();
        //buffer.write("#" + node.id + "#");
        //buffer.newLine();
        for (int i = 0; i < node.items.size(); i++)
        {
            total += node.probability.get(i);
            //buffer.write(node.relationships.get(i) + " (" + node.items.get(i).id + ");" + node.probability.get(i));
            //buffer.newLine();
        }
        average = ((float)total / (float)node.items.size());
        for (int i = 0; i < node.items.size(); i++)
        {
            stdDev += Math.pow(node.probability.get(i) - average, 2.0);
        }
        stdDev /= node.items.size();
        stdDev = (float)Math.pow(stdDev, 0.5);
        //buffer.write("Total : " + total);
        //buffer.newLine();
        //buffer.write("Average : " + average);
        //buffer.newLine();
        boolean hasResult = false;

        for (int i = 0; i < node.items.size(); i++)
        {
            if (node.probability.get(i) >= (average + stdDev))
            {
                if (!hasResult)
                {
                    hasResult = true;
                    buffer.write("#" + node.id + "#");
                    buffer.newLine();
                    buffer.write("Total : " + total);
                    buffer.newLine();
                    buffer.write("Average + Std Dev : " + (average + stdDev));
                    buffer.newLine();
                }
                buffer.write(node.relationships.get(i) + " (" + node.items.get(i).id + ");" + node.probability.get(i));
                buffer.newLine();
            }
        }
        visitedNodes.add(node.id);
        for (int i = 0; i < node.items.size(); i++)
        {
            ItemRecursiveMap(buffer, node.items.get(i), visitedNodes);
        }
    }

    public static void ItemTreeMap(String inFile, String outFile) throws IOException
    {
        List<String> visitedNodes = new ArrayList<String>();
        String[] data, session;
        String regionID;
        int regionIndex, previousIndex;
        String line;

        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        while ((line = reader.readLine()) != null)
        {
            previousIndex = -1;
            data = line.split(";");
            data = data[1].split("\\|");

            regionID = data[0];
            regionIndex = FindRegion(regionID);

            if (regionIndex == -1)
            {
                nodes.add(new Node(regionID));
                regionIndex = nodes.size() - 1;
            }

            data = data[1].split("->");
            for (int i = 0; i < data.length; i++)
            {
                session = data[i].split(",");
                int left_bracket = session[1].indexOf("[");
                int right_bracket = session[1].indexOf("]");
                String relationship = session[1].substring(0, left_bracket - 1);
                String item = session[1].substring(left_bracket + 1, right_bracket);
                if (!(item.equals(regionID) && relationship.equals("search for destination")))
                {
                    if (previousIndex == -1)
                    {
                        previousIndex = nodes.get(regionIndex).Register(item, relationship);
                    }
                    else
                    {
                        nodes.get(regionIndex).items.get(previousIndex).Register(item, relationship);
                    }
                }
            }
        }
        reader.close();

        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        float total, average, stdDev;
        float averageNumHotel = 0;
        for (int i = 0; i < nodes.size(); i++)
        {
            total = 0;
            stdDev = 0;
            nodes.get(i).CalculateProbability();
            nodes.get(i).SortProbability();
            //buffer.write("#" + nodes.get(i).id + "#");
            //buffer.newLine();
            for (int j = 0; j < nodes.get(i).items.size(); j++)
            {
                total += nodes.get(i).probability.get(j);
                //buffer.write(nodes.get(i).relationships.get(j) + " [" + nodes.get(i).items.get(j).id + "];" + nodes.get(i).probability.get(j));
                //buffer.newLine();
            }
            average = ((float)total / (float)nodes.get(i).items.size());
            for (int j = 0; j < nodes.get(i).items.size(); j++)
            {
                stdDev += Math.pow(nodes.get(i).probability.get(j) - average, 2.0);
            }
            stdDev /= nodes.get(i).items.size();
            stdDev = (float)Math.pow(stdDev, 0.5);
            //buffer.write("Total : " + total);
            //buffer.newLine();
            //buffer.write("Average : " + average);
            //buffer.newLine();
            boolean hasResult = false;
            for (int j = 0; j < nodes.get(i).items.size(); j++)
            {
                if (nodes.get(i).probability.get(j) >= (average + stdDev))
                {
                    if (!hasResult)
                    {
                        hasResult = true;
                        buffer.write("#" + nodes.get(i).id + "#");
                        buffer.newLine();
                        buffer.write("Total : " + total);
                        buffer.newLine();
                        buffer.write("Average + Std Dev : " + (average + stdDev));
                        buffer.newLine();
                    }
                    buffer.write(nodes.get(i).relationships.get(j) + " [" + nodes.get(i).items.get(j).id + "];" + nodes.get(i).probability.get(j));
                    buffer.newLine();
                }
            }
            for (int j = 0; j < nodes.get(i).items.size(); j++)
            {
                ItemRecursiveMap(buffer, nodes.get(i).items.get(j), visitedNodes);
            }
            averageNumHotel += visitedNodes.size();
            visitedNodes.clear();
        }
        averageNumHotel /= nodes.size();
        System.out.println("Average Num Hotel : " + averageNumHotel);
        System.out.println("Region : " + nodes.size());
    }

    public static List<String> LoadFile(String fileName) throws IOException {
        content = new ArrayList<String>();
        String line;
        int count = 0;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null) {
            content.add(line);
            //System.out.println(content[count]);
            count++;
        }
        reader.close();
        return content;
    }

    public static boolean SessionExist(String session, List<String> sessions) {
        for (int i = 0; i < sessions.size(); i++) {
            if (session.equals(sessions.get(i))) {
                return true;
            }
        }
        return false;
    }

    public static void SessionPredict(String inFile, String outFile, float threshold) throws IOException
    {
        String line, region;
        String[] data, session;
        int regionID;
        Node temp;
        List<String> visitedNodes = new ArrayList<String>();
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            region = data[0].split(";")[1];
            regionID = -1;
            for (int i = 0; i < nodes.size(); i++)
            {
                if (nodes.get(i).id.equals(region))
                {
                    regionID = i;
                    break;
                }
            }
            if (regionID == -1)
            {
                continue;
            }
            data = data[1].split("->");
            temp = null;
            for (int i = data.length - 1; i >= 0 ; i--)
            {
                session = data[i].split(",");
                int left_bracket = session[1].indexOf("[");
                int right_bracket = session[1].indexOf("]");
                String relationship = session[1].substring(0, left_bracket - 1);
                String item = session[1].substring(left_bracket + 1, right_bracket);
                visitedNodes.clear();
                temp = FindNode(nodes.get(regionID), item, visitedNodes);
                if (temp != null)
                {
                    break;
                }
            }
            if (temp != null)
            {

            }
            buffer.write(line);
            buffer.newLine();
        }
        buffer.close();
    }

    public static void SessionSort(String fileName, String fileName2, int boundary) throws IOException {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter writer2 = new FileWriter(fileName2);
        BufferedWriter buffer2 = new BufferedWriter(writer2);

        String[] arr, point, bites;
        List<String> sessions = new ArrayList<String>();
        List<Integer> index = new ArrayList<Integer>();
        int tempInt, step;
        String tempStr, newContent, lastSeen;

        for (int i = 0; i < content.size(); i++) {
            arr = content.get(i).split("\\|");
            sessions.clear();
            index.clear();

            point = arr[1].split("->");
            boolean proceed = true;
            for (int j = 0; j < point.length; j++) {
                if (!point[j].equals("")) {
                    bites = point[j].split(",");
                    try {
                        index.add(Integer.parseInt(bites[0]));
                        sessions.add(bites[1]);
                    } catch (Exception e) {
                        proceed = false;
                        break;
                    }
                }
            }

            if (!proceed) {
                continue;
            }

            for (int j = 0; j < index.size(); j++) {
                for (int k = j; k < index.size(); k++) {
                    if (index.get(k) < index.get(j)) {
                        tempInt = index.get(j);
                        tempStr = sessions.get(j);

                        index.set(j, index.get(k));
                        index.set(k, tempInt);

                        sessions.set(j, sessions.get(k));
                        sessions.set(k, tempStr);
                    }
                }
            }

            newContent = "";
            lastSeen = "";
            step = 1;

            for (int j = 0; j < index.size(); j++) {
                if (!sessions.get(j).equals(lastSeen)) {
                    newContent += step + "," + sessions.get(j);
                    newContent += "->";
                    step++;
                    lastSeen = sessions.get(j);
                }
            }
            newContent = newContent.substring(0, newContent.length() - 2);

            content.set(i, newContent);
            if (i < boundary)
            {
                buffer.write(arr[0] + "|" + content.get(i));
                buffer.newLine();
            }
            else
            {
                buffer2.write(arr[0] + "|" + content.get(i));
                buffer2.newLine();
            }
            System.out.println(arr[0] + "|" + content.get(i));
        }
        buffer.close();
        buffer2.close();
    }

    public static void SessionTestFilter(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            data = data[1].split("->");
            for (int i = 0; i < data.length; i++)
            {
                if (data[i].contains("clickout"))
                {
                    temp = session + "|";
                    for (int j = 0; j <= i; j++)
                    {
                        temp += data[j] + "->";
                    }
                    temp = temp.substring(0, temp.length() - 2);
                    buffer.write(temp);
                    buffer.newLine();
                }
            }
        }
        buffer.close();
    }

    public static void SessionTestHide(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            temp = session + "|";
            data = data[1].split("->");
            for (int i = 0; i < data.length - 1; i++)
            {
                temp += data[i] + "->";
            }
            if (data.length > 1)
            {
                temp = temp.substring(0, temp.length() - 2);
            }
            buffer.write(temp);
            buffer.newLine();
        }
        buffer.close();
    }

    public static void TrimDataset(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session = "", temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split(",");
            if (!session.equals(data[1]))
            {
                buffer.write(line);
                buffer.newLine();
            }
            session = data[1];
        }
        buffer.close();
    }

    public static void UserSessionCount(String fileName) {
        String[] arr;
        List<String> sessions = new ArrayList<String>();
        int count = 0;
        for (int i = 0; i < content.size(); i++) {
            arr = content.get(i).split("\\|");
            sessions.clear();
            System.out.print(arr[0] + " : ");
            for (int j = 1; j < arr.length; j++) {
                if (!SessionExist(arr[j], sessions)) {
                    System.out.print(arr[j] + ";");
                    sessions.add(arr[j]);
                }
            }
            count += sessions.size();
            System.out.println("");
        }
        float average = ((float) count / (float) content.size());
        System.out.println("Average session per user : " + average);
    }

    public static void main(String[] args) throws Exception {
        nodes = new ArrayList<Node>();

        Configuration conf = new Configuration();
        conf.set(TextOutputFormat.SEPERATOR, "|");
        FileSystem hdfs = FileSystem.get(conf);
        Job job;

        //Hotel Clickout Count
        /*job = Job.getInstance(conf, "Hotel Clickout Count (HCC)");

        job.setJarByClass(Trivago.class);
        job.setMapperClass(TokenizerMapper.class);
        job.setCombinerClass(IntSumReducer.class);
        job.setReducerClass(IntSumReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train"));
        if (hdfs.exists(new Path("trivago_train_out")))
        {
            hdfs.delete(new Path("trivago_train_out"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_out"));

        job.waitForCompletion(true);*/

        //Hotel Clickout List
        /*job = Job.getInstance(conf, "Hotel Clickout List (HCL)");

        job.setJarByClass(Trivago.class);
        job.setMapperClass(ListMapper.class);
        job.setCombinerClass(ListReducer.class);
        job.setReducerClass(ListReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train_out"));
        if (hdfs.exists(new Path("trivago_train_out_2")))
        {
            hdfs.delete(new Path("trivago_train_out_2"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_out_2"));*/

        /*job = Job.getInstance(conf, "User Session Count");

        job.setJarByClass(Trivago.class);
        job.setMapperClass(UserMapper.class);
        job.setCombinerClass(UserReducer.class);
        job.setReducerClass(UserReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train"));
        if (hdfs.exists(new Path("trivago_train_out")))
        {
            hdfs.delete(new Path("trivago_train_out"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_out"));
        job.waitForCompletion(true);*/

        //LoadFile("trivago_train_out/part-r-00000");
        //UserSessionCount("test");

        /* = Job.getInstance(conf, "Session Action List");

        job.setJarByClass(Trivago.class);
        job.setMapperClass(SessionMapper.class);
        job.setCombinerClass(SessionReducer.class);
        job.setReducerClass(SessionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train"));
        if (hdfs.exists(new Path("trivago_train_session")))
        {
            hdfs.delete(new Path("trivago_train_session"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_session"));
        job.waitForCompletion(true);

        LoadFile("trivago_train_session/part-r-00000");
        SessionSort("trivago_session/session_train.txt", "trivago_session/session_test.txt", (int)(content.size() * 0.9));
        */

        //FindMinMaxTimestamp("trivago_train/train.csv");
        //ItemTreeMap("trivago_session/session_train.txt", "trivago_session/item_above_avg_std.txt");


        //TrimDataset("trivago");
        //SessionTestFilter("trivago_session/session_test.txt", "trivago_session/session_test_filtered.txt");
        //SessionTestHide("trivago_session/session_test_filtered.txt", "trivago_session/session_test_filtered_hidden.txt");
        //SessionPredict("trivago_session/session_test_filtered_hidden.txt", "trivago_session/prediction.txt" , 0.01f);

        //SessionPredict("trivago_mini_test/prediction.txt", );

        /*job = Job.getInstance(conf, "Clicked Out Hotel List");

        job.setJarByClass(Trivago.class);
        job.setMapperClass(HotelMapper.class);
        job.setCombinerClass(HotelReducer.class);
        job.setReducerClass(HotelReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train"));
        if (hdfs.exists(new Path("trivago_train_hotel")))
        {
            hdfs.delete(new Path("trivago_train_hotel"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_hotel"));
        job.waitForCompletion(true);*/

        //hotels = LoadFile("trivago_train_hotel/part-r-00000");
        //sequence = LoadFile("trivago_train_session/session.txt");
        //HotelClickoutBasedCluster("trivago_train_session/session_hotel_cluster.txt");

        /*job = Job.getInstance(conf, "Hotel Session Clustering");

        job.setJarByClass(Trivago.class);
        job.setMapperClass(HotelClusterMapper.class);
        job.setCombinerClass(HotelClusterReducer.class);
        job.setReducerClass(HotelClusterReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_session"));
        if (hdfs.exists(new Path("trivago_session_cluster")))
        {
            hdfs.delete(new Path("trivago_session_cluster"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_session_cluster"));
        job.waitForCompletion(true);*/

        //Destination Count
        /*job = Job.getInstance(conf, "Destination Count");

        job.setJarByClass(Trivago.class);
        job.setMapperClass(DestinationMapper.class);
        job.setCombinerClass(DestinationReducer.class);
        job.setReducerClass(DestinationReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train"));
        if (hdfs.exists(new Path("trivago_destination")))
        {
            hdfs.delete(new Path("trivago_destination"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_destination"));
        job.waitForCompletion(true);*/

        //DestinationCullSession("trivago_destination/part-r-00000", "trivago_destination_mod/session_cull.txt", 2);
        DestinationMultiSession("trivago_destination_mod/session_cull.txt", "trivago_destination_mod/session_multi.txt");
    }
}