import com.sun.org.apache.xpath.internal.operations.Mult;
import org.apache.avro.generic.GenericData;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.math3.analysis.function.Multiply;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.server.namenode.FSImageFormatProtobuf;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class MarkovChain
{
    private static List<String> content;
    private static Hashtable<String, Node> dictionary = new Hashtable<String, Node>();
    private static List<Double> constants = new ArrayList<Double>();
    private static String step_delimiter = "~";
    private static String item_delimiter = ";";
    private static String comma_ex_quote_delimiter = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";

    public static class SessionActionMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            String ref, city;

            ref = "[" + data[5] + "]";
            city = data[7];

            if (data[4].equals("clickout item"))
            {
                String hotel_arr[] = data[10].split("\\|");
                String hotel_id = String.join("#", hotel_arr);

                String price_arr[] = data[11].split("\\|");
                String price_id = String.join("#", price_arr);

                ref += "[" + hotel_id + "]";
                ref += "[" + price_id + "]";
            }

            context.write(new Text(data[1] + ";" + city), new Text(data[3] + "," + data[4] + " " + ref + "->"));
        }
    }

    public static class SessionActionReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static class PriceMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            String hotel_list, price_list;
            String hotel_arr[], price_arr[];

            try
            {
                if (data[4].equals("clickout item"))
                {
                    hotel_list = data[10];
                    price_list = data[11];

                    hotel_arr = hotel_list.split("\\|");
                    price_arr = price_list.split("\\|");

                    for (int i = 0; i < hotel_arr.length; i++)
                    {
                        context.write(new Text(hotel_arr[i]), new Text(price_arr[i] + ";"));
                    }
                }
            }
            catch (Exception e)
            {
                System.out.println(line);
                System.out.println(data.length);
                System.out.println(e);
            }
        }
    }

    public static class PriceReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static class ImpressionMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");
            String hotel_list;
            String hotel_arr[];
            String clicked_hotel;

            try
            {
                if (data[4].equals("clickout item"))
                {
                    hotel_list = data[10];
                    clicked_hotel = data[5];
                    hotel_arr = hotel_list.split("\\|");

                    boolean exist = false;
                    for (int i = 0; i < hotel_arr.length; i++)
                    {
                        if (hotel_arr[i].equals(clicked_hotel))
                        {
                            exist = true;
                            break;
                        }
                    }

                    if (!exist)
                    {
                        context.write(new Text(data[0] + ";" + data[1] + ";" + data[3]), new Text(clicked_hotel + ";"));
                    }
                }
            }
            catch (Exception e)
            {
                System.out.println(line);
                System.out.println(data.length);
                System.out.println(e);
            }
        }
    }

    public static class ImpressionReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static void MapNodeRecursive(Node node, List<String> visitedNodes, Node result, int count, int depth_threshold, Double preceding_probability, Double constant, int const_index, List<Node> node_group, boolean exclusive)
    {
        if (visitedNodes.indexOf(node.id) != -1 || count > depth_threshold)
        {
            return;
        }
        else
        {
            visitedNodes.add(node.id);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            Double probability = preceding_probability * node.probability.get(i);

            result.Register(null, node.items.get(i).id, probability * constant, exclusive);

            node_group.get(count).Register(null, node.items.get(i).id, probability, exclusive);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            if (!(count + 1 > depth_threshold))
            {
                MapNodeRecursive(node.items.get(i), visitedNodes, result, count + 1, depth_threshold, preceding_probability, constants.get(const_index + 1), const_index + 1, node_group, exclusive); //Static Probability
            }
        }
    }

    public static void CalculateAccuracy(String sourceFile, String resultFile) throws IOException
    {
        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter constantsWriter = new FileWriter("constants.txt");
        BufferedWriter constantsBuffer = new BufferedWriter(constantsWriter);

        FileWriter testWriter = new FileWriter("test.txt");
        BufferedWriter testBuffer = new BufferedWriter(testWriter);

        String source_line;
        String source_id, prediction_id, region_id, session_id;
        String[] source_data;
        String nodeData;
        int left_bracket, right_bracket;
        Double count = 0.0, correct = 0.0, none = 0.0, previous = 0.0, incorrect = 0.0;
        List<String> checkedNodes = new ArrayList<>();
        List<Double> x_values = new ArrayList<Double>();
        List<List<Double>> x_matrix = new ArrayList<>();
        List<List<Double>> y_values = new ArrayList<>();
        List<Double> session_length_list = new ArrayList<>();

        int depth = 0;
        Double error = 0.0;
        int regression_index = 0;
        int limit = 999;
        Double starting_const_val = 1.0;

        while ((source_line = source.readLine()) != null)
        {
            source_data = source_line.split("\\|");
            session_id = source_data[0].split(";")[0];
            region_id = source_data[0].split(";")[1];
            source_data = source_data[1].split("->");
            source_id = source_data[source_data.length - 1];
            left_bracket = source_id.indexOf("[");
            right_bracket = source_id.indexOf("]");
            source_id = "[" + source_id.substring(left_bracket + 1, right_bracket) + "]";

            int answer_index = -1;
            Node temp;
            prediction_id = "none";

            nodeData = "none";
            Node result = new Node(session_id);

            checkedNodes.clear();
            x_values.clear();

            //Impressions Start
            nodeData = source_data[source_data.length - 1];
            nodeData = nodeData.substring(nodeData.indexOf("]") + 1);
            left_bracket = nodeData.indexOf("[");
            right_bracket = nodeData.indexOf("]");
            String impressions_list = nodeData.substring(left_bracket + 1, right_bracket);
            String[] impressions_arr = impressions_list.split("#");
            List<String> impressions = new ArrayList<>();
            List<String> answers = new ArrayList<>();

            for (int i = 0; i < impressions_arr.length; i++)
            {
                impressions.add(impressions_arr[i]);
            }
            //Impressions End

            x_values.add(1.0);
            if (constants.size() <= 0)
            {
                constants.add(starting_const_val);
            }

            String chain_name = "";
            if (source_data.length < 2)
            {
                int const_index = 1;
                List<Node> node_group = new ArrayList<>();
                for (int i = 0; i < depth + 1; i++)
                {
                    node_group.add(new Node("none"));
                }

                if (constants.size() <= const_index)
                {
                    for (int i = 0; i < depth + 1; i++)
                    {
                        constants.add(starting_const_val);
                    }
                }

                nodeData = region_id;

                temp = dictionary.get(nodeData);
                if (temp != null)
                {
                    MapNodeRecursive(temp, checkedNodes, result, 0, depth, 1.0, constants.get(const_index), const_index, node_group, false);
                }

                /*result.CalculateProbability();
                result.SortProbability();

                //Remove non-hotel_id nodes
                for (int j = 0; j < result.items.size();)
                {
                    String hotel_id = result.items.get(j).id;
                    hotel_id = hotel_id.substring(hotel_id.indexOf("[") + 1, hotel_id.indexOf("]"));
                    if (NumberUtils.isNumber(hotel_id))
                    {
                        j++;
                    }
                    else
                    {
                        result.items.remove(j);
                    }
                }

                //If not present in impressions, remove from list
                for (int j = 0; j < result.items.size();)
                {
                    boolean erase = true;
                    for (int k = 0; k < impressions.size(); k++)
                    {
                        if (result.items.get(j).id.equals("[" + impressions.get(k) + "]"))
                        {
                            impressions.remove(k);
                            erase = false;
                            break;
                        }
                    }
                    if (erase)
                    {
                        result.items.remove(j);
                    }
                    else
                    {
                        j++;
                    }
                }

                //Add remaining results to answers
                for (int j = 0; j < result.items.size(); j++)
                {
                    answers.add(result.items.get(j).id);
                }

                result.ClearData();*/
            }
            else
            {
                for (int i = source_data.length - 2, const_index = 1, const_multiplier = 1; i >= source_data.length - 2 && const_index < limit; i--, const_index += (depth + 1), const_multiplier++)
                {
                    List<Node> node_group = new ArrayList<>();
                    for (int j = 0; j < depth + 1; j++)
                    {
                        node_group.add(new Node("none"));
                    }

                    boolean exclusive = (const_multiplier != 1);

                    if (constants.size() <= const_index)
                    {
                        for (int j = 0; j < depth + 1; j++)
                        {
                            constants.add(starting_const_val);
                        }
                    }

                    nodeData = source_data[i];
                    left_bracket = nodeData.indexOf("[");
                    right_bracket = nodeData.indexOf("]");
                    String relationship = nodeData.substring(0, left_bracket - 1).split(",")[1];
                    String item = nodeData.substring(left_bracket + 1, right_bracket);
                    if (item.matches(".*[a-zA-Z]+.*"))
                    {
                        nodeData = "[" + relationship + "," + item + "," + region_id + "]";
                    }
                    else
                    {
                        nodeData = "[" + item + "]";
                    }

                    temp = dictionary.get(nodeData);
                    if (temp != null)
                    {
                        MapNodeRecursive(temp, checkedNodes, result, 0, depth, 1.0, constants.get(const_index), const_index, node_group, exclusive);
                    }

                    result.CalculateProbability();
                    result.SortProbability();

                    //Remove non-hotel_id nodes
                    for (int j = 0; j < result.items.size();)
                    {
                        String hotel_id = result.items.get(j).id;
                        hotel_id = hotel_id.substring(hotel_id.indexOf("[") + 1, hotel_id.indexOf("]"));
                        if (NumberUtils.isNumber(hotel_id))
                        {
                            j++;
                        }
                        else
                        {
                            result.items.remove(j);
                        }
                    }

                    //If not present in impressions, remove from list
                    for (int j = 0; j < result.items.size();)
                    {
                        boolean erase = true;
                        for (int k = 0; k < impressions.size(); k++)
                        {
                            if (result.items.get(j).id.equals("[" + impressions.get(k) + "]"))
                            {
                                impressions.remove(k);
                                erase = false;
                                break;
                            }
                        }
                        if (erase)
                        {
                            result.items.remove(j);
                        }
                        else
                        {
                            j++;
                        }
                    }

                    //Add remaining results to answers
                    for (int j = 0; j < result.items.size(); j++)
                    {
                        answers.add(result.items.get(j).id);
                    }

                    result.ClearData();
                }
            }

            //Add remaining impressions to answers
            for (int j = 0; j < impressions.size(); j++)
            {
                answers.add("[" + impressions.get(j) + "]");
            }

            /*result.CalculateProbability();
            result.SortProbability();

            System.out.println("Target : " + source_id);

            System.out.print("Results (Before) : ");
            for (int i = 0; i < result.items.size(); i++)
            {
                System.out.print(result.items.get(i).id + ";");
            }
            System.out.println("");

            System.out.print("Impressions : ");
            for (int i = 0; i < impressions.size(); i++)
            {
                System.out.print(impressions.get(i) + ";");
            }
            System.out.println("");

            for (int i = 0; i < result.items.size();)
            {
                String hotel_id = result.items.get(i).id;
                hotel_id = hotel_id.substring(hotel_id.indexOf("[") + 1, hotel_id.indexOf("]"));
                if (NumberUtils.isNumber(hotel_id))
                {
                    i++;
                }
                else
                {
                    result.items.remove(i);
                }
            }

            //If not present in impressions, remove from list
            for (int i = 0; i < result.items.size();)
            {
                boolean erase = true;
                for (int j = 0; j < impressions.size(); j++)
                {
                    if (result.items.get(i).id.equals("[" + impressions.get(j) + "]"))
                    {
                        impressions.remove(j);
                        erase = false;
                        break;
                    }
                }
                if (erase)
                {
                    result.items.remove(i);
                }
                else
                {
                    i++;
                }
            }

            //Insert remaining impressions
            for (int i = 0; i < impressions.size(); i++)
            {
                result.items.add(new Node("[" + impressions.get(i) + "]"));
            }

            System.out.print("Results (After) : ");
            for (int i = 0; i < result.items.size(); i++)
            {
                System.out.print(result.items.get(i).id + ";");
            }
            System.out.println("");

            for (int i = 0; i < result.items.size() && i < 25; i++)
            {
                if (result.items.get(i).id.equals(source_id))
                {
                    prediction_id = result.items.get(i).id;
                    answer_index = i;
                    break;
                }
            }*/

            for (int i = 0; i < answers.size(); i++)
            {
                if (answers.get(i).equals(source_id))
                {
                    prediction_id = answers.get(i);
                    answer_index = i;
                    break;
                }
            }

            if (answer_index != -1)
            {
                correct += (1.0 / (answer_index + 1));
                Double remaining = (1.0 - (1.0 / (answer_index + 1)));
                incorrect += remaining;
            }
            else
            {
                none++;
            }

            buffer.write(source_id + ";" + prediction_id + ";" + nodeData + ";" + chain_name);
            buffer.newLine();

            System.out.println(count);
            count++;
        }
        dictionary.clear();

        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Previous : " + (previous / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();

        buffer.close();
        testBuffer.close();
    }

    static public void SaveMatrix(List<List<Double>> matrix, String name, BufferedWriter buffer, boolean size) throws IOException
    {
        DecimalFormat df = new DecimalFormat("#.########");
        buffer.write(name);
        buffer.newLine();
        if (!size)
        {
            for (int i = 0; i < matrix.size(); i++)
            {
                for (int j = 0; j < matrix.get(i).size(); j++)
                {
                    buffer.write(df.format(matrix.get(i).get(j)) + ";");
                }
                buffer.newLine();
            }
        }
        buffer.write(matrix.size() + ";" + matrix.get(0).size());
        buffer.newLine();
    }

    static public void TransposeMatrix(List<List<Double>> matrix, List<List<Double>> temp)
    {
        for (int i = 0; i < matrix.get(0).size(); i++)
        {
            temp.add(new ArrayList<>());
            for (int j = 0; j < matrix.size(); j++)
            {
                temp.get(i).add(matrix.get(j).get(i));
            }
        }
    }

    static public void MultiplyMatrix(List<List<Double>> left, List<List<Double>> right, List<List<Double>> result)
    {
        for (int i = 0; i < left.size(); i++)
        {
            if (result.size() <= i)
            {
                result.add(new ArrayList<>());
            }

            for (int j = 0; j < right.get(0).size(); j++)
            {
                Double total = 0.0;
                for (int k = 0; k < left.get(i).size(); k++)
                {
                    total += left.get(i).get(k) * right.get(k).get(j);
                }

                total = (Math.abs(total) < 0.0001f) ? 0f : total;

                if (result.get(i).size() <= j)
                {
                    result.get(i).add(total);
                }
                else
                {
                    result.get(i).set(j, total);
                }
            }
        }
    }

    static public boolean InverseMatrix(List<List<Double>> matrix, List<List<Double>> result)
    {
        List<List<Double>> temp = new ArrayList<>();
        if (matrix.size() != matrix.get(0).size())
        {
            System.out.println("Unable to inverse the matrix");
            return false;
        }

        //Result = Identity Matrix

        //Temp Matrix
        for (int i = 0; i < matrix.size(); i++)
        {
            temp.add(new ArrayList<>());
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                temp.get(i).add(matrix.get(i).get(j));
            }
        }

        //Zero Triangle
        for (int i = 1; i < temp.size(); i++)
        {
            for (int j = i; j < temp.get(i).size(); j++)
            {
                Double multiplier;
                if (Math.abs(temp.get(i-1).get(i-1)) < 0.00000001f)
                {
                    multiplier = 0.0;
                }
                else
                {
                    multiplier = (temp.get(j).get(i-1) / temp.get(i-1).get(i-1));
                }
                for (int k = 0; k < temp.get(i).size(); k++)
                {
                    temp.get(j).set(k, temp.get(j).get(k) - temp.get(i-1).get(k) * multiplier);
                    result.get(j).set(k, result.get(j).get(k) - result.get(i-1).get(k) * multiplier);
                }
            }
        }

        double determinant = 1.0f;
        for (int i = 0; i < temp.size(); i++)
        {
            System.out.println(temp.get(i).get(i));
            determinant *= temp.get(i).get(i);
        }
        if (!(Math.abs(determinant) > 0))
        {
            return false;
        }
        System.out.println("Determinant : " + determinant);
        //The Remaining Zero(s)
        for (int i = 0; i < temp.size() - 1; i++)
        {
            for (int j = i + 1; j < temp.get(i).size(); j++)
            {
                Double multiplier;
                if (temp.get(j).get(j) == 0)
                {
                    multiplier = 0.0;
                }
                else
                {
                    multiplier = (temp.get(i).get(j) / temp.get(j).get(j));
                }
                for (int k = 0; k < temp.get(i).size(); k++)
                {
                    temp.get(i).set(k, temp.get(i).get(k) - temp.get(j).get(k) * multiplier);
                    result.get(i).set(k, result.get(i).get(k) - result.get(j).get(k) * multiplier);
                }
            }
        }

        //Simplification
        for (int i = 0; i < temp.size(); i++)
        {
            Double multiplier = temp.get(i).get(i);
            for (int j = 0; j < temp.get(i).size(); j++)
            {
                if (multiplier != 0)
                {
                    result.get(i).set(j, result.get(i).get(j) / multiplier);
                }
                else
                {
                    result.get(i).set(j, 0.0);
                }
            }
        }

        return true;
    }

    static public void LoadIdentityMatrix(List<List<Double>> matrix, List<List<Double>> result)
    {
        for (int i = 0; i < matrix.size(); i++)
        {
            if (result.size() <= i)
            {
                result.add(new ArrayList<>());
            }

            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                Double value = (i == j) ? 1.0 : 0.0;
                if (result.get(i).size() <= j)
                {
                    result.get(i).add(value);
                }
                else
                {
                    result.get(i).set(j, value);
                }
            }
        }
    }

    static public void SortMatrix(List<List<Double>> matrix, List<List<Double>> identity)
    {
        for (int k = matrix.get(0).size() - 1; k >= 0; k--)
        {
            for (int i = 0; i < matrix.size(); i++)
            {
                for (int j = i; j < matrix.size(); j++)
                {
                    if (matrix.get(j).get(k) > matrix.get(i).get(k))
                    {
                        List<Double> temp = matrix.get(i);
                        matrix.set(i, matrix.get(j));
                        matrix.set(j, temp);

                        temp = identity.get(i);
                        identity.set(i, identity.get(j));
                        identity.set(j, temp);
                    }
                }
            }
        }
    }

    static public void CullMatrix(List<List<Double>> matrix)
    {
        int abolish_index;
        for (int i = 0; i < matrix.size(); i++)
        {
            if (!(matrix.get(i).get(i) > 0 || matrix.get(i).get(i) < 0))
            {
                abolish_index = i;
                break;
            }
        }

    }

    static public Double GetDeterminant(List<List<Double>> matrix, int row, int column)
    {
        if (matrix.size() == 2)
        {
            return (matrix.get(0).get(0) * matrix.get(1).get(1)) - (matrix.get(0).get(1) * matrix.get(1).get(0));
        }

        Double total = 0.0;
        for (int i = column; i < matrix.get(row).size(); i++)
        {
            total += GetDeterminant(matrix, (row + 1), i);
        }
        return total;
    }

    public static void RecalculateAccuracy(String sourceFile, String resultFile) throws IOException
    {
        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        String source_line;
        String[] source_data;
        Double count = 0.0, correct = 0.0, none = 0.0, previous = 0.0, incorrect = 0.0;
        while ((source_line = source.readLine()) != null)
        {
            if (source_line.contains(";"))
            {
                source_data = source_line.split(";");
                if (source_data[0].contains("["))
                {
                    if (source_data[0].equals(source_data[1]))
                    {
                        //System.out.println(source_data[0] + " " + source_data[1]);
                        correct++;
                    }
                    else
                    {
                        if (source_data[1].equals("none"))
                        {
                            none++;
                        }
                        else
                        {
                            incorrect++;
                        }
                    }
                    buffer.write(source_line);
                    buffer.newLine();
                    count++;
                }
            }
        }
        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Previous : " + (previous / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();
        buffer.close();
    }

    public static void ItemTreeMap(String inFile, String outFile) throws IOException
    {
        dictionary.clear();

        String[] data, session;
        String regionID;
        String line;
        Node temp, existing;
        int count = 0;
        int window_limit = 2;

        System.out.println("Loading to list..");
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        List<String> multi_order = new ArrayList<>();
        while ((line = reader.readLine()) != null)
        {
            data = line.split(";");
            data = data[1].split("\\|");
            regionID = data[0];

            int window = 1;
            String[] temp_arr = data[1].split("->");

            //System.out.println(line);
            while(window < window_limit)
            {
                String item_arr = regionID + "|";
                boolean save = false;
                for (int i = 0; i <= temp_arr.length - window; i++)
                {
                    save = true;
                    String relationship = "action";
                    String hotel_id = window + step_delimiter + relationship + "[";
                    int hotel_count = 0;
                    for (int j = 0; j < window; j++)
                    {
                        String sub_relationship = temp_arr[i + j].substring(temp_arr[i + j].indexOf(",") + 1, temp_arr[i + j].indexOf("[") - 1);
                        String clean_id = temp_arr[i + j].substring(temp_arr[i + j].indexOf("[") + 1, temp_arr[i + j].indexOf("]"));
                        if ((clean_id.equals(regionID) && sub_relationship.equals("search for destination")))
                        {
                            continue;
                        }
                        hotel_count++;
                        String final_id = (clean_id.matches(".*[a-zA-Z]+.*")) ? (sub_relationship + "," + clean_id + "," + regionID) : clean_id;
                        hotel_id += final_id;
                        if (j + 1 < window)
                        {
                            hotel_id += ";";
                        }
                    }
                    hotel_id += "]";
                    if (hotel_count < window)
                    {
                        continue;
                    }
                    //System.out.println(hotel_id);
                    item_arr += hotel_id;
                    if (i + 1 <= temp_arr.length - window)
                    {
                        item_arr += "->";
                    }
                }
                if (save)
                {
                    //System.out.println("Window #" + window + ": ");
                    //System.out.println(item_arr);
                    multi_order.add(item_arr);
                }
                window++;
            }
            count++;
            System.out.println(count);
        }
        reader.close();

        for (int j = 0; j < multi_order.size(); j++)
        {
            System.out.println(j);
            data = multi_order.get(j).split("\\|");
            regionID = data[0];
            Node region_node = dictionary.get(regionID);

            if (region_node == null)
            {
                dictionary.put(regionID, new Node(regionID));
            }

            if (data.length != 2)
            {
                continue;
            }

            //System.out.println(data[1]);
            data = data[1].split("->");
            temp = dictionary.get(regionID);

            //System.out.println(multi_order.get(j));
            for (int i = 0; i < data.length; i++)
            {
                //System.out.println(data[i]);
                session = data[i].split(step_delimiter);
                int window = Integer.parseInt(session[0]);
                int left_bracket = session[1].indexOf("[");
                int right_bracket = session[1].indexOf("]");
                String relationship = session[1].substring(0, left_bracket - 1);
                String item = session[1].substring(left_bracket + 1, right_bracket);
                String item_combined = "[" + item + "]";
                String first_item = "[" + item.split(item_delimiter)[window - 1] + "]";

                if (!(item.equals(regionID) && relationship.equals("search for destination")))
                {
                    existing = dictionary.get(first_item);
                    if (existing == null)
                    {
                        existing = new Node(first_item);
                        dictionary.put(first_item, existing);
                    }
                    temp.Register(existing, first_item, 1.0, false);

                    Node current_node = dictionary.get(item_combined);
                    if (current_node == null)
                    {
                        current_node = new Node(item_combined);
                        dictionary.put(item_combined, current_node);
                    }
                    temp = current_node;
                }
            }
        }

        System.out.println("Mapping graph..");

        System.out.println("Saving graph to file..");
        SaveGraphToFile(outFile);
    }

    public static void LoadFile(String fileName) throws IOException
    {
        content = new ArrayList<String>();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null) {
            content.add(line);
        }
        reader.close();
    }

    public static void LoadConstants(String fileName) throws IOException
    {
        constants.clear();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null)
        {
            if (line.contains(";"))
            {
                String[] values = line.split(";");
                for (String value : values)
                {
                    constants.add(Double.parseDouble(value));
                }
            }
        }
        reader.close();
    }

    public static void LoadMatrix(List<List<Double>> matrix, String fileName) throws IOException
    {
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        while ((line = reader.readLine()) != null)
        {
            matrix.add(new ArrayList<>());
            if (line.contains(";"))
            {
                String[] values = line.split(";");
                for (String value : values)
                {
                    matrix.get(matrix.size() - 1).add(Double.parseDouble(value));
                }
            }
        }
        reader.close();
    }

    public static void CopyMatrix(List<List<Double>> source, List<List<Double>> destination)
    {
        for (int i = 0; i < source.size(); i++)
        {
            if (destination.size() <= i)
            {
                destination.add(new ArrayList<>());
            }

            for (int j = 0; j < source.get(i).size(); j++)
            {
                if (destination.get(i).size() <= j)
                {
                    destination.get(i).add(0.0);
                }
                destination.get(i).set(j, source.get(i).get(j));
            }
        }
    }

    public static void PrintMatrix(List<List<Double>> matrix)
    {
        DecimalFormat df = new DecimalFormat("#.########");
        for (int i = 0; i < matrix.size(); i++)
        {
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                System.out.print(df.format(matrix.get(i).get(j)) + ";");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    public static void SaveGraphToFile(String fileName) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);
        Set<String> keys = dictionary.keySet();

        Node node;
        Double total, average, stdDev;
        for(String key: keys)
        {
            node = dictionary.get(key);
            total = 0.0;
            stdDev = 0.0;

            node.CalculateProbability();
            node.SortProbability();
            for (int i = 0; i < node.items.size(); i++)
            {
                total += node.probability.get(i);
            }

            average = (node.items.size() > 0) ? total / (double)node.items.size() : 0.0;
            for (int i = 0; i < node.items.size(); i++)
            {
                stdDev += Math.pow((node.probability.get(i) - average), 2.0);
            }
            stdDev = (node.items.size() > 0) ? stdDev / node.items.size() : 0;
            stdDev = (Double)Math.pow(stdDev, 0.5);

            buffer.write("#" + node.id + "#");
            buffer.newLine();
            buffer.write("Total : " + total);
            buffer.newLine();
            buffer.write("Average + Std Dev : " + (average + stdDev));
            buffer.newLine();
            for (int i = 0; i < node.items.size(); i++)
            {
                buffer.write(node.items.get(i).id + ";" + node.probability.get(i));
                buffer.newLine();
            }
        }
        buffer.close();
    }

    public static void SessionSort(String fileName, String fileName2, int boundaryTop, int boundaryBottom) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter writer2 = new FileWriter(fileName2);
        BufferedWriter buffer2 = new BufferedWriter(writer2);

        String[] arr, point, bites;
        List<String> sessions = new ArrayList<String>();
        List<Integer> index = new ArrayList<Integer>();
        int tempInt, step;
        String tempStr, newContent, lastSeen;

        for (int i = 0; i < content.size(); i++)
        {
            arr = content.get(i).split("\\|");
            sessions.clear();
            index.clear();

            point = arr[1].split("->");
            boolean proceed = true;
            for (int j = 0; j < point.length; j++) {
                if (!point[j].equals("")) {
                    bites = point[j].split(comma_ex_quote_delimiter);
                    try {
                        index.add(Integer.parseInt(bites[0]));
                        sessions.add(bites[1]);
                    } catch (Exception e) {
                        proceed = false;
                        break;
                    }
                }
            }

            if (!proceed) {
                continue;
            }

            for (int j = 0; j < index.size(); j++)
            {
                for (int k = j; k < index.size(); k++)
                {
                    if (index.get(k) < index.get(j))
                    {
                        tempInt = index.get(j);
                        tempStr = sessions.get(j);

                        index.set(j, index.get(k));
                        index.set(k, tempInt);

                        sessions.set(j, sessions.get(k));
                        sessions.set(k, tempStr);
                    }
                }
            }

            newContent = "";
            lastSeen = "";
            step = 1;

            for (int j = 0; j < index.size(); j++)
            {
                if (!sessions.get(j).equals(lastSeen))
                {
                    newContent += step + "," + sessions.get(j);
                    newContent += "->";
                    step++;
                    lastSeen = sessions.get(j);
                }
            }
            newContent = newContent.substring(0, newContent.length() - 2);

            if (i >= boundaryTop && i < boundaryBottom)
            {
                buffer2.write(arr[0] + "|" + newContent);
                buffer2.newLine();
            }
            else
            {
                buffer.write(arr[0] + "|" + newContent);
                buffer.newLine();
            }
        }
        buffer.close();
        buffer2.close();

        content.clear();
    }

    public static void SessionTestFilter(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            data = data[1].split("->");
            for (int i = 0; i < data.length; i++)
            {
                if (data[i].contains("clickout"))
                {
                    temp = session + "|";
                    for (int j = 0; j <= i; j++)
                    {
                        temp += data[j] + "->";
                    }
                    temp = temp.substring(0, temp.length() - 2);
                    buffer.write(temp);
                    buffer.newLine();
                }
            }
        }
        buffer.close();
    }

    public static void SessionTestHide(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            temp = session + "|";
            data = data[1].split("->");
            for (int i = 0; i < data.length - 1; i++)
            {
                temp += data[i] + "->";
            }
            if (data.length > 1)
            {
                temp = temp.substring(0, temp.length() - 2);
            }
            buffer.write(temp);
            buffer.newLine();
        }
        buffer.close();
    }

    public static void main(String[] args) throws Exception
    {
        long start = System.currentTimeMillis();

        Configuration conf = new Configuration();
        conf.set(TextOutputFormat.SEPERATOR, "|");
        FileSystem hdfs = FileSystem.get(conf);
        Job job;

        /*job = Job.getInstance(conf, "Session Action List");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(SessionActionMapper.class);
        job.setCombinerClass(SessionActionReducer.class);
        job.setReducerClass(SessionActionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train/train.csv"));
        if (hdfs.exists(new Path("trivago_train_session")))
        {
            hdfs.delete(new Path("trivago_train_session"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_session"));
        job.waitForCompletion(true);*/

        /*job = Job.getInstance(conf, "Price Mapper");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(PriceMapper.class);
        job.setCombinerClass(PriceReducer.class);
        job.setReducerClass(PriceReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train/train.csv"));
        if (hdfs.exists(new Path("trivago_price")))
        {
            hdfs.delete(new Path("trivago_price"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_price"));
        job.waitForCompletion(true);*/

        /*job = Job.getInstance(conf, "Impression Mapper");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(ImpressionMapper.class);
        job.setCombinerClass(ImpressionReducer.class);
        job.setReducerClass(ImpressionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train/train.csv"));
        if (hdfs.exists(new Path("trivago_impression")))
        {
            hdfs.delete(new Path("trivago_impression"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_impression"));
        job.waitForCompletion(true);*/

        Double boundaryTop = 0.8;
        Double boundaryBottom = 0.9;
        Double boundaryLimit = 0.9;
        int fileIndex = 137;

        String session_train, session_test, session_probability, session_test_filtered;
        String session_test_filtered_hidden, session_prediction, session_accuracy;

        //Matrix Function Testing
        List<List<Double>> matrix = new ArrayList<>();
        List<List<Double>> original_matrix = new ArrayList<>();
        List<List<Double>> transpose = new ArrayList<>();
        List<List<Double>> interim = new ArrayList<>();
        List<List<Double>> interim_original = new ArrayList<>();
        List<List<Double>> interim_inv = new ArrayList<>();
        List<List<Double>> interim_check = new ArrayList<>();
        List<List<Double>> y_matrix = new ArrayList<>();
        List<List<Double>> inverse = new ArrayList<>();
        List<List<Double>> multiply = new ArrayList<>();
        List<List<Double>> result = new ArrayList<>();

        LoadMatrix(matrix, "matrix.txt");
        //PrintMatrix(matrix);

        //Check Linear Regression Start
        LoadMatrix(y_matrix, "y_matrix.txt");
        //PrintMatrix(y_matrix);

        Double error = 0.0;
        for (int i = 0; i < matrix.size(); i++)
        {
            Double total = 0.0;
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                total += matrix.get(i).get(j);
            }
            error += Math.pow(y_matrix.get(i).get(0) - total, 2.0f);
        }
        error /= matrix.size();
        System.out.println("Error Before : " + error);

        TransposeMatrix(matrix, transpose);
        //PrintMatrix(transpose);

        MultiplyMatrix(transpose, matrix, interim);
        PrintMatrix(interim);

        LoadIdentityMatrix(interim, inverse);
        PrintMatrix(inverse);

        SortMatrix(interim, inverse);
        //PrintMatrix(interim);
        //PrintMatrix(inverse);

        if (InverseMatrix(interim, inverse))
        {
            System.out.println("Invertible Matrix");
        }
        else
        {
            System.out.println("Non-Invertible Matrix");
        }
        //PrintMatrix(inverse);

        MultiplyMatrix(inverse, transpose, interim);
        //PrintMatrix(interim);

        MultiplyMatrix(interim, y_matrix, result);
        //PrintMatrix(result);

        error = 0.0;
        for (int i = 0; i < matrix.size(); i++)
        {
            Double total = 0.0;
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                total += matrix.get(i).get(j) * result.get(j).get(0);
            }
            Double single_error = (Double)Math.pow(y_matrix.get(i).get(0) - total, 2.0f);
            error += single_error;
        }
        error /= matrix.size();
        System.out.println("Error After: " + error);
        //Check Linear Regression End

        //Check Everything Start
        /*TransposeMatrix(matrix, transpose);
        PrintMatrix(transpose);

        MultiplyMatrix(transpose, matrix, interim);
        CopyMatrix(interim, interim_original);
        LoadIdentityMatrix(interim, interim_inv);
        SortMatrix(interim, interim_inv);
        InverseMatrix(interim, interim_inv);
        MultiplyMatrix(interim, interim_inv, interim_check);
        PrintMatrix(interim_check);*/
        //Check Everything End

        //Check Multiplication Start
        /*LoadMatrix(y_matrix, "y_matrix.txt");
        PrintMatrix(y_matrix);

        CopyMatrix(matrix, original_matrix);
        PrintMatrix(original_matrix);

        LoadIdentityMatrix(matrix, inverse);
        PrintMatrix(inverse);

        SortMatrix(matrix, inverse);
        PrintMatrix(matrix);
        PrintMatrix(inverse);

        InverseMatrix(matrix, inverse);
        PrintMatrix(inverse);

        MultiplyMatrix(original_matrix, inverse, multiply);
        PrintMatrix(multiply);

        MultiplyMatrix(inverse, y_matrix, result);
        PrintMatrix(result);*/
        //Check Multiplication End

        LoadConstants("constants.txt");

        while (boundaryBottom <= boundaryLimit)
        {
            LoadFile("trivago_train_session/part-r-00000");
            System.out.println("Iteration : " + fileIndex);

            session_train = "trivago_session/session_train" + fileIndex + ".txt";
            session_test = "trivago_session/session_test" + fileIndex + ".txt";
            session_probability = "trivago_session/probability" + fileIndex + ".txt";
            session_test_filtered = "trivago_session/session_test_filtered" + fileIndex + ".txt";
            session_test_filtered_hidden = "trivago_session/session_test_filtered_hidden" + fileIndex + ".txt";
            session_prediction = "trivago_session/prediction" + fileIndex + ".txt";
            session_accuracy = "trivago_session/accuracy" + fileIndex + ".txt";

            SessionSort(session_train, session_test, (int)(content.size() * boundaryTop), (int)(content.size() * boundaryBottom));
            ItemTreeMap(session_train, session_probability);
            SessionTestFilter(session_test, session_test_filtered);
            SessionTestHide(session_test_filtered, session_test_filtered_hidden);
            CalculateAccuracy(session_test_filtered, session_accuracy);
            boundaryTop += 0.1f;
            boundaryBottom += 0.1f;
            fileIndex++;
            RecalculateAccuracy("trivago_session/accuracy" + (fileIndex - 1) + ".txt", "trivago_session/accuracy" + (fileIndex - 1) + "_recalculated.txt");
        }
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
    }
}
