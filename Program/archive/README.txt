0 - 9 : Cross Validation using MRR using 1st Order Markov Chain, 25 (max) results considered, Node also considers relationship
10 : 90th percentile data with bold MRR (MRR always return 1 if found at any index) using 1st Order Markov Chain, 25 (max) results considered
11 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered
12 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered
13 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 10)
14 & 15 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 100)
16 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 20)
17 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 10)
18 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 3)
19 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 1)
20 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 2)
21 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 2) [Fixed Accuracy Algorithm]
22 - 31 : Cross Validation using MRR using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 2) [Fixed Accuracy Algorithm]
32 - 41 : Cross Validation using MRR using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 0) [Fixed Accuracy Algorithm]
42 - 51 : Cross Validation using MRR using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : -1) [Fixed Accuracy Algorithm]
52 :  90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 0) [Fixed Accuracy Algorithm] + Region Check (if it exists in the region)
53 : 90th percentile data with bold MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 0) [Fixed Accuracy Algorithm] + bold Cosine Similarity
54 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered, previous nodes considered (BFS depth : 0)
55 - 65 : ???
66 - 99 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, AMAP results considered, previous nodes considered (BFS depth : 0), Linear Regression with starting learning_rate = 1.0f, increasing by 5% if error reduces and decreased by 50% if error increases
100 : ???
101 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0)
102 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0) with Linear Regression (Inverse) But it's wrong
103 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0)
104 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0)
105 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0)
106 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0)
107 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0) with Linear Regression (Inverse) and 1.0 constant (without 1.0f)
108 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0) with Linear Regression (Inverse) and trained constant from 107 (without 1.0f)
109 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0) with Linear Regression (Inverse) and 1.0 constant (with 1.0f)
110 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0) with Linear Regression (Inverse) and trained constant from 109 (with 1.0f)
111 : idunno
112 : Inverse testing stuff
113 : 90th percentile data with MRR and bold Node (node ignores action type) using 1st Order Markov Chain, 25 results considered (BFS depth : 0) with Linear Regression (Inverse) and 1.0 constant (with 1.0f), tested on the same 90% data used for training
114 : Back to Markov Chain
115 - 123 : Cross Validation Markov Chain MRR 25 results, Node = hotel_id, only check last node for prediction
124 : 80 - 90th percentile, Window_Size = 2 (Wrong)
125 : 80 - 90th percentile, Window_Size = 1 (Correct)
126 : 80 - 90th percentile, Window_Size = 2 (Correct)
127 : 80 - 90th percentile, Window_Size = 3 (Correct)
128 : 80 - 90th percentile, Window_Size = 2 (Correct) - Cumulative (Plus)
129 : 80 - 90th percentile, Window_Size = 3 (Correct) - Cumulative (Plus)
130 : 90th - 100th percentile, Window_Size = 1 (Correct)
131 : 90th - 100th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id
132 : 90th - 100th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations
133 : 90th - 100th percentile, Window_Size = 2 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations
134 : 90th - 100th percentile, previous nodes considered, Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations
135 : 90th - 100th percentile, previous nodes considered (exclusive), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations
*exclusive : only added to stack if it already exists on the first node
136 : 90th - 100th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations, ignore if chain does not have previous actions
137 : 80th - 90th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations, ignore if chain does not have previous actions
138 : 90th - 100th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations, ignore if chain does not have previous actions, consider previous nodes using stack
e.g.
LastNode : A, B, C
2ndLastNode : D, E
3rd Last Node : F
Remaining Impressions : G, H
Result : A, B, C, D, E, F, G, H
139 : 90th - 100th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations, ignore if chain does not have previous actions, remaining impressions sort by distance from average_price
140 : 90th - 100th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations, ignore if chain does not have previous actions, remaining impressions sort by price_segments
price_segments example :
44 -> <= 44
70 -> <= 70
90 -> <= 90
141 : 90th - 100th percentile, Window_Size = 1 (Correct), Remove if not within Impressions or is not a hotel_id, impressions stack to fill empty recommendations, ignore if chain does not have previous actions, padded with seen_hotels, remaining impressions sort by distance from average_price
142 : 90th - 100th percentile, padded with seen_hotels
143 : 90th - 100th percentile, padded with seen_hotels, remaining impressions sort by distance from average_price
144 : 
first seen -> Accuracy : 0.573575862559343
first seen, all hotel -> Accuracy : 0.5559233955539258
first seen, all hotel, avg_dist asc -> Accuracy : 0.5529070447268192
all seen -> Accuracy : 0.575
all seen, all hotel -> Accuracy : 0.5621017163566511
all seen, all hotel, avg_dist asc -> Accuracy : 0.5593391056354541

Accuracy : 0.574016868528194 -> first seen, ignore non-hotel nodes
Accuracy : 0.5654763654845304 -> first seen, ignore non-hotel nodes, added markov
Accuracy : 0.5647813324767313 -> first seen, ignore non-hotel nodes, sort by dist_from_avg_price (only last hotel as price)
Accuracy : 0.5643258416821871 -> first seen, ignore non-hotel nodes, added markov, sort by dist_from_avg_price (only last hotel as price, only when markov is present)
Accuracy : 0.5633430384526694 -> first seen, ignore non-hotel nodes, added markov, sort by dist_from_avg_price (only last hotel as price)
Accuracy : 0.575894279753473 -> all seen, ignore non-hotel nodes
Accuracy : 0.5515879430759533 -> exclude seen, added markov, sort by dist_from_avg_price (only last hotel as price)
Accuracy : 0.553039057725151 -> added markov, first seen, ignore non-hotel nodes, sort by dist_from_avg_price
Accuracy : 0.5429808383783711 -> exclude seen, added markov, sort by dist_from_avg_price (as many as possible)
Accuracy : 0.5761396192222598 (BiS) -> all seen, ignore non-hotel nodes, added markov (1), limit 4
Accuracy : 0.5766607257982364 (True BiS) -> all seen, ignore non-hotel nodes, added markov (1), sort by dist_from_avg_price, limit 4
Accuracy : 0.5765787438392901

Accuracy : 0.5766350891910899


Accuracy : 0.5515879430759533

Addition of region-based hotel check :
With base : 0.8925326
Nothing : 0.8914965
With chain : 0.919874
With base & chain : 0.92091006

Legend :
1. trivago_impression -> Checking sessions where clicked out item is not listed in the impressions list
2. trivago_price -> Checking price of each hotel in the impression list


1. Possibility of using action_type as hidden layer
2. Possibility of immature chain,
example : A -> B -> C -> D -> E
	A' -> B' -> C' -> D' -> E'
A, .., E : item_type
A', .., E' : action_type
It might be better to stop the chain at C and calculate the probability from A->B->C
*Find out % of answers found in previous nodes
3. Find the node that produces the result, find the relation between that node, the chain, and the answer
4. Calculate the sum of probabilities from all the preceding nodes
e.g. if the chain is A->B->C
and A->B,C
    B->B,D
    C->D,E

then value of B = P(B|A) + P(B|B)
			  C = P(C|A)
			  D = P(D|B) + P(D|C)
			  E = P(E|C)
5. Use tag similarity as multiplier, 1/D, where D = JaccardSimilarity
6. Combination with Linear Regression Model where score = a0x0 + a1x1 + a2x2 + ... + anxn
   an = nth constant
   xn = probability value of nth node of the chain, calculated from the tail
   
   e.g. A->B->C->D

   x4 = D, x3 = C, x2 = B, x1 = A

   if nth node does not exist, give score of 0

   NB. This may tackle problem number 2
7. Linear Regression Model
a0 + a1x1 + a2x2 + ... + anxn = y
xn = probability of the sought after hotel on nth node, where n is calculated from the back of the chain
e.g., in A->B->C->D, n=0 is D, n=1 is C, n=2 is B, n=3 is A
y = highest probability in the chain, however this causes problem as y is calculated from x (even in the beginning), considering of replacing it with 1.0f (dependent predictor)

another thing to consider, find the highest dimension possible by calculating the determinants
increase the dimension as long as the resultant determinant is not 0
also, try using the training set to calculate the constants

8. using price ratio to weight the impressions
1/(dist_from_chain_avg_price)
e.g. avg_price = 100
price = 50
1/dist = 1/(100-50)
sequence-aware multiorder might be too specific, instead try combination multiorder

9. Cluster using GDBScan (?), then find major cluster for the chain, and only calculate probability from hotels that originate in major clusters

10. Remove items not included in impressions
Target : [6107]
Results (Before) : [6100];[15800];[152191];[10246024];[6108];[1514585];[51881];[filter selection,Swimming Pool (Combined Filter),"Celle, Germany"];[change of sort order,interaction sort button,"Celle, Germany"];[6107];[6106];[filter selection,Very Good Rating,"Celle, Germany"];[filter selection,Hotel,"Celle, Germany"];[filter selection,Breakfast Included,"Celle, Germany"];[6110];[6104];[6105];[52870];[filter selection,Pet Friendly,"Celle, Germany"];
Impressions : 6100;15800;6111;10351216;10293386;9674504;7972274;939066;6107;5645304;4264442;
Results (After) : [6100];[15800];[6107];

11. Answer Length (Average) : 22.910428790304294 -> Includes impressions
Accuracy : 0.5293803870949866
Answer Length (Average) : 3.280056888058317 -> Without impressions & not including country when chain is empty
Accuracy : 0.27282535532659624

(BiS) -> Accuracy : 0.533665421526019 -> order by dist_from_avg_seen_price ascending
Chain with known Average Price : 0.5807047798335956

Total count : 145486.0
1st rank count : 60018
Rest count : 85468

(1st rank only) -> 60018/145486.0 = 0.4125345394
Average of rest of the group = (0.533665421526019 * 145486.0 - 1.0 * 60018) / 85468 = 0.20619234703 -> 85468 averaging on 5th spot
Proofcheck : (60018 + (0.20619234703 * 85468)) / 145486.0 = 0.53366542152

Track Record (Impressions Only) :     Track Record : 
#0 : 46368                            #0 : 60018
#1 : 15535                            #1 : 13616
#2 : 11027                            #2 : 9348
#3 : 8601                             #3 : 7406
#4 : 7539                             #4 : 6370
#5 : 5999                             #5 : 5378
#6 : 5292                             #6 : 4714
#7 : 4570                             #7 : 4181
#8 : 4180                             #8 : 3704
#9 : 3776                             #9 : 3500
#10 : 3463                            #10 : 3040
#11 : 3164                            #11 : 2770
#12 : 2891                            #12 : 2588
#13 : 2707                            #13 : 2350
#14 : 2436                            #14 : 2127
#15 : 2284                            #15 : 2037
#16 : 2153                            #16 : 1811
#17 : 1991                            #17 : 1664
#18 : 1794                            #18 : 1558
#19 : 1790                            #19 : 1385
#20 : 1636                            #20 : 1348
#21 : 1560                            #21 : 1206
#22 : 1523                            #22 : 1184
#23 : 1509                            #23 : 1051
#24 : 1698                            #24 : 1132

Track Record (With Markov Chain) : 
#0 : 59052
#1 : 13830
#2 : 9669
#3 : 7641
#4 : 6614
#5 : 5583
#6 : 4859
#7 : 4161
#8 : 3725
#9 : 3394
#10 : 3008
#11 : 2793
#12 : 2497
#13 : 2346
#14 : 2094
#15 : 1921
#16 : 1688
#17 : 1622
#18 : 1476
#19 : 1397
#20 : 1319
#21 : 1210
#22 : 1172
#23 : 1115
#24 : 1300

Track Record (With Markov Chain and ORDER BY dist_from_avg_price ASC) : 
#0 : 60018
#1 : 13616
#2 : 9348
#3 : 7406
#4 : 6370
#5 : 5378
#6 : 4714
#7 : 4181
#8 : 3704
#9 : 3500
#10 : 3040
#11 : 2770
#12 : 2588
#13 : 2350
#14 : 2127
#15 : 2037
#16 : 1811
#17 : 1664
#18 : 1558
#19 : 1385
#20 : 1348
#21 : 1206
#22 : 1184
#23 : 1051
#24 : 1132

Cumulative Track Record (Descending) : 
#24 : 1132.0
#23 : 2183.0
#22 : 3367.0
#21 : 4573.0
#20 : 5921.0
#19 : 7306.0
#18 : 8864.0
#17 : 10528.0
#16 : 12339.0
#15 : 14376.0
#14 : 16503.0
#13 : 18853.0
#12 : 21441.0
#11 : 24211.0
#10 : 27251.0
#9 : 30751.0
#8 : 34455.0
#7 : 38636.0
#6 : 43350.0
#5 : 48728.0
#4 : 55098.0
#3 : 62504.0
#2 : 71852.0
#1 : 85468.0
#0 : 145486.0

Cumulative Track Record (Ascending): 
#0 : 60018.0
#1 : 73634.0
#2 : 82982.0
#3 : 90388.0
#4 : 96758.0
#5 : 102136.0
#6 : 106850.0
#7 : 111031.0
#8 : 114735.0
#9 : 118235.0
#10 : 121275.0
#11 : 124045.0
#12 : 126633.0
#13 : 128983.0
#14 : 131110.0
#15 : 133147.0
#16 : 134958.0
#17 : 136622.0
#18 : 138180.0
#19 : 139565.0
#20 : 140913.0
#21 : 142119.0
#22 : 143303.0
#23 : 144354.0
#24 : 145486.0


Accuracy
None
Previous
Incorrect
Answer Length (Average)
Chain with known Average Price
Seen Hotels (Average)
Added Seen Hotels (Average) 
Average rank with Markov


//Seen (1), Markov Chain (1)
//limit = 0             //limit = 1                   //limit = 2                   //limit = 3             //limit = 4             //limit = 5     
0.5654763654845304      0.5731715523863882            0.5740457099567824            0.574283476159994       0.5743553826394218      0.5743379378684376                
4.3284392197816545E-4   4.3284392197816545E-4         4.3284392197816545E-4         4.3284392197816545E-4   4.3284392197816545E-4   4.3284392197816545E-4                       
0.0                     0.0                           0.0                           0.0                     0.0                     0.0       
0.4340907905934814      0.4263956036916218            0.4255214461212209            0.42528367991800703     0.4252117734385779      0.42522921820956194                  
2.9328679688627197      3.7449243897244227            4.570893650935424             5.410095569189758       6.256484070656617       7.108362132340312               
0.5651155281039375      0.5651155281039375            0.5651155281039375            0.5651155281039375      0.5651155281039375      0.5651155281039375                  
0.6399219506832751      0.6399219506832751            0.6399219506832751            0.6399219506832751      0.6399219506832751      0.6399219506832751                  
0.5651155281039375      0.5651155281039375            0.5651155281039375            0.5651155281039375      0.5651155281039375      0.5651155281039375                  
0.1473368467753512      0.1195348992722456            0.30949316424640516           0.3795615247058639      0.4420580821963286      0.5065112073275582                  
Track Record : 
#0 : 65731              #0 : 66415                    #0 : 66415                    #0 : 66415              #0 : 66415              #0 : 66415
#1 : 12591              #1 : 14516                    #1 : 14608                    #1 : 14608              #1 : 14608              #1 : 14608
#2 : 9049               #2 : 8476                     #2 : 8935                     #2 : 8932               #2 : 8932               #2 : 8932
#3 : 7118               #3 : 6671                     #3 : 6677                     #3 : 6798               #3 : 6751               #3 : 6751
#4 : 6099               #4 : 5761                     #4 : 5702                     #4 : 5876               #4 : 5893               #4 : 5852
#5 : 5163               #5 : 4811                     #5 : 4718                     #5 : 4750               #5 : 4947               #5 : 4757
#6 : 4460               #6 : 4254                     #6 : 4124                     #6 : 4071               #6 : 4098               #6 : 4369
#7 : 3846               #7 : 3660                     #7 : 3588                     #7 : 3534               #7 : 3526               #7 : 3566
#8 : 3429               #8 : 3329                     #8 : 3280                     #8 : 3204               #8 : 3149               #8 : 3127
#9 : 3069               #9 : 2959                     #9 : 2926                     #9 : 2881               #9 : 2853               #9 : 2863
#10 : 2749              #10 : 2685                    #10 : 2639                    #10 : 2605              #10 : 2560              #10 : 2556
#11 : 2573              #11 : 2498                    #11 : 2474                    #11 : 2471              #11 : 2484              #11 : 2462
#12 : 2308              #12 : 2265                    #12 : 2256                    #12 : 2226              #12 : 2205              #12 : 2204
#13 : 2132              #13 : 2103                    #13 : 2077                    #13 : 2075              #13 : 2040              #13 : 2026
#14 : 1925              #14 : 1894                    #14 : 1899                    #14 : 1879              #14 : 1878              #14 : 1869
#15 : 1776              #15 : 1751                    #15 : 1741                    #15 : 1746              #15 : 1739              #15 : 1725
#16 : 1558              #16 : 1553                    #16 : 1546                    #16 : 1537              #16 : 1537              #16 : 1537
#17 : 1502              #17 : 1495                    #17 : 1492                    #17 : 1488              #17 : 1489              #17 : 1493
#18 : 1334              #18 : 1322                    #18 : 1322                    #18 : 1324              #18 : 1321              #18 : 1315
#19 : 1325              #19 : 1324                    #19 : 1323                    #19 : 1322              #19 : 1321              #19 : 1320
#20 : 1238              #20 : 1236                    #20 : 1236                    #20 : 1235              #20 : 1234              #20 : 1232
#21 : 1133              #21 : 1132                    #21 : 1132                    #21 : 1133              #21 : 1130              #21 : 1131
#22 : 1074              #22 : 1073                    #22 : 1073                    #22 : 1073              #22 : 1073              #22 : 1073
#23 : 1050              #23 : 1049                    #23 : 1049                    #23 : 1049              #23 : 1049              #23 : 1049
#24 : 1254              #24 : 1254                    #24 : 1254                    #24 : 1254              #24 : 1254              #24 : 1254

Cumulative Track Record : 
#0 : 65731.0            #0 : 66415.0                  #0 : 66415.0                  #0 : 66415.0            #0 : 66415.0            #0 : 66415.0
#1 : 78322.0            #1 : 80931.0                  #1 : 81023.0                  #1 : 81023.0            #1 : 81023.0            #1 : 81023.0
#2 : 87371.0            #2 : 89407.0                  #2 : 89958.0                  #2 : 89955.0            #2 : 89955.0            #2 : 89955.0
#3 : 94489.0            #3 : 96078.0                  #3 : 96635.0                  #3 : 96753.0            #3 : 96706.0            #3 : 96706.0
#4 : 100588.0           #4 : 101839.0                 #4 : 102337.0                 #4 : 102629.0           #4 : 102599.0           #4 : 102558.0
#5 : 105751.0           #5 : 106650.0                 #5 : 107055.0                 #5 : 107379.0           #5 : 107546.0           #5 : 107315.0
#6 : 110211.0           #6 : 110904.0                 #6 : 111179.0                 #6 : 111450.0           #6 : 111644.0           #6 : 111684.0
#7 : 114057.0           #7 : 114564.0                 #7 : 114767.0                 #7 : 114984.0           #7 : 115170.0           #7 : 115250.0
#8 : 117486.0           #8 : 117893.0                 #8 : 118047.0                 #8 : 118188.0           #8 : 118319.0           #8 : 118377.0
#9 : 120555.0           #9 : 120852.0                 #9 : 120973.0                 #9 : 121069.0           #9 : 121172.0           #9 : 121240.0
#10 : 123304.0          #10 : 123537.0                #10 : 123612.0                #10 : 123674.0          #10 : 123732.0          #10 : 123796.0
#11 : 125877.0          #11 : 126035.0                #11 : 126086.0                #11 : 126145.0          #11 : 126216.0          #11 : 126258.0
#12 : 128185.0          #12 : 128300.0                #12 : 128342.0                #12 : 128371.0          #12 : 128421.0          #12 : 128462.0
#13 : 130317.0          #13 : 130403.0                #13 : 130419.0                #13 : 130446.0          #13 : 130461.0          #13 : 130488.0
#14 : 132242.0          #14 : 132297.0                #14 : 132318.0                #14 : 132325.0          #14 : 132339.0          #14 : 132357.0
#15 : 134018.0          #15 : 134048.0                #15 : 134059.0                #15 : 134071.0          #15 : 134078.0          #15 : 134082.0
#16 : 135576.0          #16 : 135601.0                #16 : 135605.0                #16 : 135608.0          #16 : 135615.0          #16 : 135619.0
#17 : 137078.0          #17 : 137096.0                #17 : 137097.0                #17 : 137096.0          #17 : 137104.0          #17 : 137112.0
#18 : 138412.0          #18 : 138418.0                #18 : 138419.0                #18 : 138420.0          #18 : 138425.0          #18 : 138427.0
#19 : 139737.0          #19 : 139742.0                #19 : 139742.0                #19 : 139742.0          #19 : 139746.0          #19 : 139747.0
#20 : 140975.0          #20 : 140978.0                #20 : 140978.0                #20 : 140977.0          #20 : 140980.0          #20 : 140979.0
#21 : 142108.0          #21 : 142110.0                #21 : 142110.0                #21 : 142110.0          #21 : 142110.0          #21 : 142110.0
#22 : 143182.0          #22 : 143183.0                #22 : 143183.0                #22 : 143183.0          #22 : 143183.0          #22 : 143183.0
#23 : 144232.0          #23 : 144232.0                #23 : 144232.0                #23 : 144232.0          #23 : 144232.0          #23 : 144232.0
#24 : 145486.0          #24 : 145486.0                #24 : 145486.0                #24 : 145486.0          #24 : 145486.0          #24 : 145486.0

//Seen (1), Markov Chain (1), Sort
//limit = 0             //limit = 1                   //limit = 2                   //limit = 3             //limit = 4             //limit = 5     
0.5633430384526694      0.5735483994903228            0.5747923402232874            0.575036261987211       0.575044125159324       0.5749272093544977  
4.3284392197816545E-4   4.3284392197816545E-4         4.3284392197816545E-4         4.3284392197816545E-4   4.3284392197816545E-4   4.3284392197816545E-4
0.0                     0.0                           0.0                           0.0                     0.0                     0.0  
0.4362241176253398      0.4260187565876862            0.4247748158547176            0.42453089409079153     0.424523030918679       0.4246399467235031  
2.9328679688627197      3.7449243897244227            4.570893650935424             5.410095569189758       6.256484070656617       7.108362132340312    
0.5651155281039375      0.5651155281039375            0.5651155281039375            0.5651155281039375      0.5651155281039375      0.5651155281039375    
0.6399219506832751      0.6399219506832751            0.6399219506832751            0.6399219506832751      0.6399219506832751      0.6399219506832751  
0.5651155281039375      0.5651155281039375            0.5651155281039375            0.5651155281039375      0.5651155281039375      0.5651155281039375  
0.14785279109972926     0.1195625725911072            0.3094946320105803            0.37956299247007813     0.4420580821960802      0.506511207327303  
Track Record : 
#0 : 65731              #0 : 66415                    #0 : 66415                    #0 : 66415              #0 : 66415              #0 : 66415
#1 : 12151              #1 : 14516                    #1 : 14608                    #1 : 14608              #1 : 14608              #1 : 14608
#2 : 8743               #2 : 8484                     #2 : 8935                     #2 : 8932               #2 : 8932               #2 : 8932
#3 : 7000               #3 : 6837                     #3 : 6880                     #3 : 6798               #3 : 6751               #3 : 6751
#4 : 6036               #4 : 5781                     #4 : 5912                     #4 : 6129               #4 : 5893               #4 : 5852
#5 : 5028               #5 : 4851                     #5 : 4851                     #5 : 5023               #5 : 5240               #5 : 4757
#6 : 4404               #6 : 4198                     #6 : 4155                     #6 : 4214               #6 : 4339               #6 : 4650
#7 : 3946               #7 : 3702                     #7 : 3643                     #7 : 3606               #7 : 3748               #7 : 3873
#8 : 3449               #8 : 3396                     #8 : 3330                     #8 : 3311               #8 : 3274               #8 : 3341
#9 : 3193               #9 : 3056                     #9 : 3075                     #9 : 3048               #9 : 3059               #9 : 3031
#10 : 2829              #10 : 2716                    #10 : 2692                    #10 : 2718              #10 : 2701              #10 : 2750
#11 : 2626              #11 : 2494                    #11 : 2472                    #11 : 2482              #11 : 2503              #11 : 2534
#12 : 2360              #12 : 2229                    #12 : 2164                    #12 : 2164              #12 : 2180              #12 : 2234
#13 : 2226              #13 : 2105                    #13 : 2076                    #13 : 2052              #13 : 2035              #13 : 2040
#14 : 2025              #14 : 1908                    #14 : 1878                    #14 : 1823              #14 : 1834              #14 : 1824
#15 : 1912              #15 : 1827                    #15 : 1741                    #15 : 1738              #15 : 1665              #15 : 1670
#16 : 1676              #16 : 1595                    #16 : 1560                    #16 : 1496              #16 : 1498              #16 : 1444
#17 : 1545              #17 : 1445                    #17 : 1447                    #17 : 1429              #17 : 1435              #17 : 1454
#18 : 1428              #18 : 1335                    #18 : 1278                    #18 : 1263              #18 : 1276              #18 : 1282
#19 : 1316              #19 : 1244                    #19 : 1212                    #19 : 1182              #19 : 1140              #19 : 1161
#20 : 1304              #20 : 1221                    #20 : 1194                    #20 : 1166              #20 : 1170              #20 : 1125
#21 : 1183              #21 : 1092                    #21 : 1038                    #21 : 1033              #21 : 984               #21 : 989
#22 : 1160              #22 : 1031                    #22 : 997                     #22 : 972               #22 : 971               #22 : 955
#23 : 1039              #23 : 966                     #23 : 936                     #23 : 922               #23 : 889               #23 : 887
#24 : 1176              #24 : 1042                    #24 : 997                     #24 : 962               #24 : 946               #24 : 927
Cumulative Track Record : 
#0 : 65731.0            #0 : 66415.0                  #0 : 66415.0                  #0 : 66415.0            #0 : 66415.0            #0 : 66415.0
#1 : 77882.0            #1 : 80931.0                  #1 : 81023.0                  #1 : 81023.0            #1 : 81023.0            #1 : 81023.0
#2 : 86625.0            #2 : 89415.0                  #2 : 89958.0                  #2 : 89955.0            #2 : 89955.0            #2 : 89955.0
#3 : 93625.0            #3 : 96252.0                  #3 : 96838.0                  #3 : 96753.0            #3 : 96706.0            #3 : 96706.0
#4 : 99661.0            #4 : 102033.0                 #4 : 102750.0                 #4 : 102882.0           #4 : 102599.0           #4 : 102558.0
#5 : 104689.0           #5 : 106884.0                 #5 : 107601.0                 #5 : 107905.0           #5 : 107839.0           #5 : 107315.0
#6 : 109093.0           #6 : 111082.0                 #6 : 111756.0                 #6 : 112119.0           #6 : 112178.0           #6 : 111965.0
#7 : 113039.0           #7 : 114784.0                 #7 : 115399.0                 #7 : 115725.0           #7 : 115926.0           #7 : 115838.0
#8 : 116488.0           #8 : 118180.0                 #8 : 118729.0                 #8 : 119036.0           #8 : 119200.0           #8 : 119179.0
#9 : 119681.0           #9 : 121236.0                 #9 : 121804.0                 #9 : 122084.0           #9 : 122259.0           #9 : 122210.0
#10 : 122510.0          #10 : 123952.0                #10 : 124496.0                #10 : 124802.0          #10 : 124960.0          #10 : 124960.0
#11 : 125136.0          #11 : 126446.0                #11 : 126968.0                #11 : 127284.0          #11 : 127463.0          #11 : 127494.0
#12 : 127496.0          #12 : 128675.0                #12 : 129132.0                #12 : 129448.0          #12 : 129643.0          #12 : 129728.0
#13 : 129722.0          #13 : 130780.0                #13 : 131208.0                #13 : 131500.0          #13 : 131678.0          #13 : 131768.0
#14 : 131747.0          #14 : 132688.0                #14 : 133086.0                #14 : 133323.0          #14 : 133512.0          #14 : 133592.0
#15 : 133659.0          #15 : 134515.0                #15 : 134827.0                #15 : 135061.0          #15 : 135177.0          #15 : 135262.0
#16 : 135335.0          #16 : 136110.0                #16 : 136387.0                #16 : 136557.0          #16 : 136675.0          #16 : 136706.0
#17 : 136880.0          #17 : 137555.0                #17 : 137834.0                #17 : 137986.0          #17 : 138110.0          #17 : 138160.0
#18 : 138308.0          #18 : 138890.0                #18 : 139112.0                #18 : 139249.0          #18 : 139386.0          #18 : 139442.0
#19 : 139624.0          #19 : 140134.0                #19 : 140324.0                #19 : 140431.0          #19 : 140526.0          #19 : 140603.0
#20 : 140928.0          #20 : 141355.0                #20 : 141518.0                #20 : 141597.0          #20 : 141696.0          #20 : 141728.0
#21 : 142111.0          #21 : 142447.0                #21 : 142556.0                #21 : 142630.0          #21 : 142680.0          #21 : 142717.0
#22 : 143271.0          #22 : 143478.0                #22 : 143553.0                #22 : 143602.0          #22 : 143651.0          #22 : 143672.0
#23 : 144310.0          #23 : 144444.0                #23 : 144489.0                #23 : 144524.0          #23 : 144540.0          #23 : 144559.0
#24 : 145486.0          #24 : 145486.0                #24 : 145486.0                #24 : 145486.0          #24 : 145486.0          #24 : 145486.0

Accuracy : 0.2610345352549845
None : 0.609588523452583
Previous : 0.0
Incorrect : 0.12937694129241475
Answer Length (Average) : 4.452823447773602
Chain with known Average Price : 0.5651155281039375
Seen Hotels (Average) : 1.7386034943558526
Added Seen Hotels (Average) : 0.0
Average rank with Markov : 0.33775859818430953

Accuracy : 0.5261062693706938
None : 4.3284392197816545E-4
Previous : 0.0
Incorrect : 0.4734608867073108
Answer Length (Average) : 4.452823447773602
Chain with known Average Price : 0.5651155281039375
Seen Hotels (Average) : 1.7386034943558526
Added Seen Hotels (Average) : 0.0
Average rank with Markov : 0.3377909890011287

Accuracy : 0.5515879430759533
None : 4.3284392197816545E-4
Previous : 0.0
Incorrect : 0.44797921300205124
Answer Length (Average) : 2.8334856302688443
Chain with known Average Price : 0.5651155281039375
Seen Hotels (Average) : 1.7386034943558526
Added Seen Hotels (Average) : 0.0
Average rank with Markov : 0.14785279109974703

Accuracy : 0.5349436647813255
None : 0.09438058660657235
Previous : 0.0
Incorrect : 0.370675748612088
Answer Length (Average) : 0.5339164130292892
Chain with known Average Price : 0.5651155281039375
Seen Hotels (Average) : 1.7386034943558526
Added Seen Hotels (Average) : 0.0
Average rank with Markov : 0.028058597671752238

//Markov onlys                                              //Seen All Only
Accuracy : 0.2644757452123172                               Accuracy : 0.30168081517409323                            
None : 0.6711416773732557                                   None : 0.6742402902115439                        
Previous : 0.0                                              Previous : 0.0                                                       
Incorrect : 0.06438257741442273                             Incorrect : 0.02407889461436297                                                      
Answer Length (Average) : 0.0                               Average rank with Markov : 0.2331235830287327                            
Chain with known Average Price : 0.5651155281039375         Average Markov length : 33.51859991671679                                                  
Seen Hotels (Average) : 0.17571402070780287                 Markov Accuracy : 0.9264209568524802                                            
Added Seen Hotels (Average) : 0.0                           Average rank with Seen : 0.23412916016366475                                                   
Average rank with Markov : 0.9806539224903374               Average Seen length : 3.5506601425739235                                                   
Average Markov length : 8.616128695288833                   Seen Accuracy : 0.92608387749977                                                            
Markov Accuracy : 0.8042239682420882                        Track Record :                                                               
Track Record :                                                    
#0 : 34191                                                  #0 : 41512
#1 : 4613                                                   #1 : 3519
#2 : 2685                                                   #2 : 1166
#3 : 1758                                                   #3 : 554
#4 : 1165                                                   #4 : 312
#5 : 882                                                    #5 : 131
#6 : 650                                                    #6 : 77
#7 : 473                                                    #7 : 53
#8 : 351                                                    #8 : 35
#9 : 293                                                    #9 : 23
#10 : 229                                                   #10 : 14
#11 : 152                                                   #11 : 7
#12 : 116                                                   #12 : 5
#13 : 96                                                    #13 : 2
#14 : 59                                                    #14 : 1
#15 : 56                                                    #15 : 1
#16 : 31                                                    #16 : 1
#17 : 23                                                    #17 : 1
#18 : 17                                                    #18 : 0
#19 : 10                                                    #19 : 0
#20 : 11                                                    #20 : 0
#21 : 3                                                     #21 : 0
#22 : 1                                                     #22 : 0
#23 : 0                                                     #23 : 0
#24 : 0                                                     #24 : 0                                                  
Cumulative Track Record :                                                     
#0 : 34191.0                                                #0 : 41512.0                                                   
#1 : 38804.0                                                #1 : 45031.0                                                   
#2 : 41489.0                                                #2 : 46197.0                                                   
#3 : 43247.0                                                #3 : 46751.0                                                   
#4 : 44412.0                                                #4 : 47063.0                                                   
#5 : 45294.0                                                #5 : 47194.0                                                   
#6 : 45944.0                                                #6 : 47271.0                                                   
#7 : 46417.0                                                #7 : 47324.0                                                   
#8 : 46768.0                                                #8 : 47359.0                                                   
#9 : 47061.0                                                #9 : 47382.0                                                   
#10 : 47290.0                                               #10 : 47396.0                                                   
#11 : 47442.0                                               #11 : 47403.0                                                   
#12 : 47558.0                                               #12 : 47408.0                                                   
#13 : 47654.0                                               #13 : 47410.0                                                   
#14 : 47713.0                                               #14 : 47411.0                                                   
#15 : 47769.0                                               #15 : 47412.0                                                   
#16 : 47800.0                                               #16 : 47413.0                                                   
#17 : 47823.0                                               #17 : 47414.0                                                   
#18 : 47840.0                                               #18 : 47414.0                                                   
#19 : 47850.0                                               #19 : 47414.0                                                   
#20 : 47861.0                                               #20 : 47414.0                                                   
#21 : 47864.0                                               #21 : 47414.0                                                   
#22 : 47865.0                                               #22 : 47414.0                                                   
#23 : 47865.0                                               #23 : 47414.0                                                   
#24 : 47865.0                                               #24 : 47414.0

//Markov Only, include when Markov exist but answer is not within Markov
Accuracy : 0.2644757452123172
None : 0.6711416773732557
Previous : 0.0
Incorrect : 0.06438257741442273
Average rank with Markov : 0.6040200229053801
Average Markov length : 5.306983567319942
Markov Accuracy : 0.4953504682722852
Average rank with Seen : NaN
Average Seen length : Infinity
Seen Accuracy : NaN
Markov prob avg : NaN
Track Record : 
#0 : 34191
#1 : 4613
#2 : 2685
#3 : 1758
#4 : 1165
#5 : 882
#6 : 650
#7 : 473
#8 : 351
#9 : 293
#10 : 229
#11 : 152
#12 : 116
#13 : 96
#14 : 59
#15 : 56
#16 : 31
#17 : 23
#18 : 17
#19 : 10
#20 : 11
#21 : 3
#22 : 1
#23 : 0
#24 : 0
#25 : 29846
Cumulative Track Record : 
#0 : 34191.0
#1 : 38804.0
#2 : 41489.0
#3 : 43247.0
#4 : 44412.0
#5 : 45294.0
#6 : 45944.0
#7 : 46417.0
#8 : 46768.0
#9 : 47061.0
#10 : 47290.0
#11 : 47442.0
#12 : 47558.0
#13 : 47654.0
#14 : 47713.0
#15 : 47769.0
#16 : 47800.0
#17 : 47823.0
#18 : 47840.0
#19 : 47850.0
#20 : 47861.0
#21 : 47864.0
#22 : 47865.0
#23 : 47865.0
#24 : 47865.0
#25 : 77711.0                                                   



//Markov Additive All
Accuracy : 0.279458702056569
None : 0.609588523452583
Previous : 0.0
Incorrect : 0.11095277449082334
Answer Length (Average) : 0.0
Chain with known Average Price : 0.5807047798335956
Seen Hotels (Average) : 0.04766779572515098
Added Seen Hotels (Average) : 0.0
Average rank with Markov : 1.8197592566521188
Average Markov length : 11.405462480641983
Markov Accuracy : 0.7158055509227009
Track Record : 
#0 : 34767
#1 : 5389
#2 : 3579
#3 : 2607
#4 : 1965
#5 : 1630
#6 : 1304
#7 : 1043
#8 : 900
#9 : 743
#10 : 584
#11 : 511
#12 : 407
#13 : 348
#14 : 264
#15 : 228
#16 : 143
#17 : 112
#18 : 104
#19 : 74
#20 : 53
#21 : 34
#22 : 20
#23 : 13
#24 : 2
Cumulative Track Record : 
#0 : 34767.0
#1 : 40156.0
#2 : 43735.0
#3 : 46342.0
#4 : 48307.0
#5 : 49937.0
#6 : 51241.0
#7 : 52284.0
#8 : 53184.0
#9 : 53927.0
#10 : 54511.0
#11 : 55022.0
#12 : 55429.0
#13 : 55777.0
#14 : 56041.0
#15 : 56269.0
#16 : 56412.0
#17 : 56524.0
#18 : 56628.0
#19 : 56702.0
#20 : 56755.0
#21 : 56789.0
#22 : 56809.0
#23 : 56822.0
#24 : 56824.0

//Markov Additive 2
Accuracy : 0.2725748488619953
None : 0.6427045187531347
Previous : 0.0
Incorrect : 0.08472063238485761
Answer Length (Average) : 0.0
Chain with known Average Price : 0.5725288390851191
Seen Hotels (Average) : 0.09878460174923909
Added Seen Hotels (Average) : 0.0
Average rank with Markov : 1.3086685639566187
Average Markov length : 9.815110376124913
Markov Accuracy : 0.7628835604379385
Track Record : 
#0 : 34540
#1 : 5089
#2 : 3198
#3 : 2215
#4 : 1572
#5 : 1251
#6 : 969
#7 : 722
#8 : 570
#9 : 459
#10 : 350
#11 : 282
#12 : 214
#13 : 166
#14 : 119
#15 : 99
#16 : 56
#17 : 42
#18 : 39
#19 : 21
#20 : 22
#21 : 5
#22 : 3
#23 : 1
#24 : 0
Cumulative Track Record : 
#0 : 34540.0
#1 : 39629.0
#2 : 42827.0
#3 : 45042.0
#4 : 46614.0
#5 : 47865.0
#6 : 48834.0
#7 : 49556.0
#8 : 50126.0
#9 : 50585.0
#10 : 50935.0
#11 : 51217.0
#12 : 51431.0
#13 : 51597.0
#14 : 51716.0
#15 : 51815.0
#16 : 51871.0
#17 : 51913.0
#18 : 51952.0
#19 : 51973.0
#20 : 51995.0
#21 : 52000.0
#22 : 52003.0
#23 : 52004.0
#24 : 52004.0

For train data :
1. All Seen : 0.5732318581223019
2. Impressions Only : 0.457
3. Markov (1st Order, last hotel) : 0.657
4. Markov + Sort : 0.656
5. Markov + Last Seen : 0.619
5. All Seen + Markov + Sort : 0.636