# Introduction

Recommender system using Trivago Dataset for thesis purposes.

# Tools

1. Java SE Development Kit 8
2. Hadoop 2.6.0
3. Intellij IDEA 2019.2

*The tools mentioned above are the ones used in the compilation of the version shared in this repository. The code base has also been compiled with JDK 12 and Hadoop 3.1.2 and run successfully without any changes.

# Dataset

The dataset used in this application is Trivago's train.csv provided by [ACM RecSys Challenge 2019](http://www.recsyschallenge.com/2019/).

The train.csv is not provided in this repository, instead its pre-processed form can be found in [trivago_train_session/part-r-00000](https://gitlab.com/Miscavel/trivago-graph-analysis/blob/master/Program/trivago_train_session/part-r-00000).

The pre-processed data is to be split into train and test data with a ratio of x : y that can be set in the main section of the program.

# Algorithm

The system puts an emphasis in the usage of Markov Chain in producing a recommendation list based on the hotels and filters that the user have interacted with throughout the session.

The program supports certain variations of the Markov Model, such as Multi-Order and Multi-Depth Markov Chain, while the current build uses 1st Order Markov Chain with zero depth as it produces the best result.

The model also considers price similarity and the impressions list provided at the moment of the clickout in generating the recommendation list.

# Steps

1. Pre-process Trivago's train.csv located at [trivago_train/trivago.csv](https://gitlab.com/Miscavel/trivago-graph-analysis/blob/master/Program/trivago_train/train.csv) using Hadoop's MapReduce with {key: session_id, value: relevant_session_data}.
2. Clean the pre-processed data from duplicate entries, and re-arrange the action index shuffled by the MapReduce.
3. Split the clean pre-processed data into train and test data according to the x : y ratio set by the user.
4. Map the train data into an n-Order Markov Model, n corresponds to the value of the window_limit parameter.
5. Use the generated Markov Model to produce recommendation lists for each entry in the test data.
6. Evaluate the relevance of the recommendation lists using Mean Reciprocal Rank metrics as suggested by [ACM RecSys Challenge 2019](http://www.recsyschallenge.com/2019/).

# Results

At the end of the process, 4 files will be generated in the [trivago_session](https://gitlab.com/Miscavel/trivago-graph-analysis/tree/master/Program/trivago_session) directory with the following names (followed by index) and descriptions

1. Accuracy : contains the MRR score of the model
2. Probability : contains the probability model used for the Markov Model
3. Session Test : contains the data used for testing the model
4. Session Test Filtered : test data that has been filtered to only include entries that end in clickouts, as well as splitting entries that contain multiple clickouts
5. Session Train : contains the data used to map the Markov Model

# Evaluation

Overall, on average the model produces an MRR score of 0.575 when used on the test data, and 0.65 when used on the train data. However, since the test and train data is derived from the same source (train.csv), the actual MRR score might deviate from these results.

