import java.util.ArrayList;
import java.util.List;

public class Node
{
    public String id;
    public List<Node> items;
    public List<String> relationships;
    public List<Double> count;
    public List<Double> probability;

    public Node(String _id)
    {
        id = _id;
        items = new ArrayList<Node>();
        relationships = new ArrayList<String>();
        count = new ArrayList<Double>();
        probability = new ArrayList<Double>();
    }

    void CalculateProbability()
    {
        Double totalCount = 0;
        Double prob;
        for (int i = 0; i < count.size(); i++)
        {
            totalCount += count.get(i);
        }
        for (int i = 0; i < items.size(); i++)
        {
            prob = (Double)count.get(i) / (Double)totalCount;
            probability.add(prob);
        }
    }

    int FindCombination(String item, String relationship)
    {
        for (int i = 0; i < items.size(); i++)
        {
            if (items.get(i).id.equals(item) && relationships.get(i).equals(relationship))
            {
                return i;
            }
        }
        return -1;
    }

    public Node Register(Node node, String item, String relationship, Double value)
    {
        int combinationIndex = FindCombination(item, relationship);
        if (combinationIndex == -1)
        {
            if (node == null)
            {
                items.add(new Node(item));
            }
            else
            {
                items.add(node);
            }
            relationships.add(relationship);
            count.add(0f);
            combinationIndex = items.size() - 1;
        }
        count.set(combinationIndex, count.get(combinationIndex) + value);
        return items.get(combinationIndex);
    }

    public void SortProbability()
    {
        Node temp;
        String tempRelationship;
        Double tempCount;
        Double tempProbability;
        for (int i = 0; i < items.size(); i++)
        {
            for (int j = i; j < items.size(); j++)
            {
                if (probability.get(j) > probability.get(i))
                {
                    temp = items.get(i);
                    tempRelationship = relationships.get(i);
                    tempCount = count.get(i);
                    tempProbability = probability.get(i);

                    items.set(i, items.get(j));
                    relationships.set(i, relationships.get(j));
                    count.set(i, count.get(j));
                    probability.set(i, probability.get(j));

                    items.set(j, temp);
                    relationships.set(j, tempRelationship);
                    count.set(j, tempCount);
                    probability.set(j, tempProbability);
                }
            }
        }
    }
}
