import java.util.ArrayList;
import java.util.List;

public class Node
{
    public String id;
    public List<Node> items;
    public List<Double> count;
    public List<Double> probability;

    public Node(String _id)
    {
        id = _id;
        items = new ArrayList<Node>();
        count = new ArrayList<Double>();
        probability = new ArrayList<Double>();
    }

    void CalculateProbability()
    {
        Double totalCount = 0.0;
        Double prob;

        for (int i = 0; i < count.size(); i++)
        {
            totalCount += count.get(i);
        }

        for (int i = 0; i < items.size(); i++)
        {
            prob = count.get(i) / totalCount;
            probability.add(prob);
        }
    }

    int FindCombination(String item)
    {
        for (int i = 0; i < items.size(); i++)
        {
            if (items.get(i).id.equals(item))
            {
                return i;
            }
        }
        return -1;
    }

    public Node Register(Node node, String item, Double value)
    {
        int combinationIndex = FindCombination(item);
        if (combinationIndex == -1)
        {
            if (node == null)
            {
                items.add(new Node(item));
            }
            else
            {
                items.add(node);
            }
            count.add(0.0);
            combinationIndex = items.size() - 1;
        }
        count.set(combinationIndex, count.get(combinationIndex) + value);
        return items.get(combinationIndex);
    }

    public void SortProbability()
    {
        Node temp;
        Double tempCount;
        Double tempProbability;
        for (int i = 0; i < items.size(); i++)
        {
            for (int j = i; j < items.size(); j++)
            {
                if (probability.get(j) > probability.get(i))
                {
                    temp = items.get(i);
                    tempCount = count.get(i);
                    tempProbability = probability.get(i);

                    items.set(i, items.get(j));
                    count.set(i, count.get(j));
                    probability.set(i, probability.get(j));

                    items.set(j, temp);
                    count.set(j, tempCount);
                    probability.set(j, tempProbability);
                }
            }
        }
    }
}
