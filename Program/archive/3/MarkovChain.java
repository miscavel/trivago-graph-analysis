import com.sun.org.apache.xpath.internal.operations.Mult;
import org.apache.avro.generic.GenericData;
import org.apache.commons.math3.analysis.function.Multiply;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hdfs.server.namenode.FSImageFormatProtobuf;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import java.io.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

public class MarkovChain
{
    private static List<String> content;
    private static List<Node> nodes = new ArrayList<Node>();
    private static Hashtable<String, Node> dictionary = new Hashtable<String, Node>();
    private static List<Double> constants = new ArrayList<Double>();

    public static class SessionActionMapper extends Mapper<Object, Text, Text, Text>
    {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException
        {
            String line = value.toString();
            String data[] = line.split(",");
            String ref, city;

            if (data[4].equals("search for destination"))
            {
                ref = data[5] + data[6];
                city = data[8] + data[9];
            }
            else
            {
                ref = data[5];
                city = data[7] + data[8];
            }

            context.write(new Text(data[1] + ";" + city), new Text(data[3] + "," + data[4] + " [" + ref + "]->"));
        }
    }

    public static class SessionActionReducer extends Reducer<Text, Text, Text, Text>
    {
        public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException
        {
            String value = "";
            for (Text val : values)
            {
                value += val.toString();
            }
            context.write(key, new Text(value));
        }
    }

    public static void MapNodeRecursive(Node node, List<String> visitedNodes, Node result, int count, int depth_threshold, Double preceding_probability, Double constant, int const_index, List<Node> node_group)
    {
        if (visitedNodes.indexOf(node.id) != -1 || count > depth_threshold)
        {
            return;
        }
        else
        {
            visitedNodes.add(node.id);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            Double probability = preceding_probability * node.probability.get(i);
            result.Register(null, node.items.get(i).id, probability * constant);
            node_group.get(count).Register(null, node.items.get(i).id, probability);
        }

        for (int i = 0; i < node.items.size(); i++)
        {
            if (!(count + 1 > depth_threshold))
            {
                MapNodeRecursive(node.items.get(i), visitedNodes, result, count + 1, depth_threshold, preceding_probability, constants.get(const_index + 1), const_index + 1, node_group); //Static Probability
            }
        }
    }

    public static void CalculateAccuracy(String sourceFile, String resultFile) throws IOException
    {
        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter constantsWriter = new FileWriter("constants.txt");
        BufferedWriter constantsBuffer = new BufferedWriter(constantsWriter);

        FileWriter testWriter = new FileWriter("test.txt");
        BufferedWriter testBuffer = new BufferedWriter(testWriter);

        String source_line;
        String source_id, prediction_id, region_id, session_id;
        String[] source_data;
        String nodeData;
        int left_bracket, right_bracket;
        Double count = 0.0, correct = 0.0, none = 0.0, previous = 0.0, incorrect = 0.0;
        List<String> checkedNodes = new ArrayList<>();
        List<Double> x_values = new ArrayList<Double>();
        List<List<Double>> x_matrix = new ArrayList<>();
        List<List<Double>> y_values = new ArrayList<>();
        List<Double> session_length_list = new ArrayList<>();

        int depth = 0;
        Double error = 0.0;
        int regression_index = 0;
        int limit = 999;
        Double starting_const_val = 1.0;

        while ((source_line = source.readLine()) != null)
        {
            source_data = source_line.split("\\|");
            session_id = source_data[0].split(";")[0];
            region_id = source_data[0].split(";")[1];
            source_data = source_data[1].split("->");
            source_id = source_data[source_data.length - 1];
            left_bracket = source_id.indexOf("[");
            right_bracket = source_id.indexOf("]");
            source_id = "[" + source_id.substring(left_bracket + 1, right_bracket) + "]";

            int answer_index = -1;
            Node temp;
            prediction_id = "none";

            nodeData = "none";
            Node result = new Node(session_id);

            checkedNodes.clear();
            x_values.clear();

            x_values.add(1.0);
            if (constants.size() <= 0)
            {
                constants.add(starting_const_val);
            }

            if (source_data.length < 2)
            {
                int const_index = 1;
                List<Node> node_group = new ArrayList<>();
                for (int i = 0; i < depth + 1; i++)
                {
                    node_group.add(new Node("none"));
                }

                if (constants.size() <= const_index)
                {
                    for (int i = 0; i < depth + 1; i++)
                    {
                        constants.add(starting_const_val);
                    }
                }

                nodeData = region_id;

                temp = dictionary.get(nodeData);
                if (temp != null)
                {
                    MapNodeRecursive(temp, checkedNodes, result, 0, depth, 1.0, constants.get(const_index), const_index, node_group);
                }

                for (int j = 0; j < node_group.size(); j++)
                {
                    int temp_index = -1;
                    for (int k = 0; k < node_group.get(j).items.size(); k++)
                    {
                        if (node_group.get(j).items.get(k).id.equals(source_id))
                        {
                            temp_index = k;
                            break;
                        }
                    }
                    if (temp_index > -1)
                    {
                        x_values.add(node_group.get(j).count.get(temp_index));
                    }
                    else
                    {
                        x_values.add(0.0);
                    }
                }
            }
            else
            {
                for (int i = source_data.length - 2, const_index = 1; i >= source_data.length - 2 && const_index < limit; i--, const_index += (depth + 1))
                {
                    List<Node> node_group = new ArrayList<>();
                    for (int j = 0; j < depth + 1; j++)
                    {
                        node_group.add(new Node("none"));
                    }

                    if (constants.size() <= const_index)
                    {
                        for (int j = 0; j < depth + 1; j++)
                        {
                            constants.add(starting_const_val);
                        }
                    }
                    nodeData = source_data[i];
                    left_bracket = nodeData.indexOf("[");
                    right_bracket = nodeData.indexOf("]");
                    String relationship = nodeData.substring(0, left_bracket - 1).split(",")[1];
                    String item = nodeData.substring(left_bracket + 1, right_bracket);
                    if (item.matches(".*[a-zA-Z]+.*"))
                    {
                        nodeData = "[" + relationship + "," + item + "," + region_id + "]";
                    }
                    else
                    {
                        nodeData = "[" + item + "]";
                    }

                    temp = dictionary.get(nodeData);
                    if (temp != null)
                    {
                        MapNodeRecursive(temp, checkedNodes, result, 0, depth, 1.0, constants.get(const_index), const_index, node_group);
                    }

                    for (int j = 0; j < node_group.size(); j++)
                    {
                        int temp_index = -1;
                        for (int k = 0; k < node_group.get(j).items.size(); k++)
                        {
                            if (node_group.get(j).items.get(k).id.equals(source_id))
                            {
                                temp_index = k;
                                break;
                            }
                        }
                        if (temp_index > -1)
                        {
                            x_values.add(node_group.get(j).count.get(temp_index));
                        }
                        else
                        {
                            x_values.add(0.0);
                        }
                    }
                }
            }

            result.CalculateProbability();
            result.SortProbability();

            Double base = 0.0;
            for (int i = 0; i < result.items.size(); i++)
            {
                base += result.count.get(i);
            }

            Double x_total = 0.0;
            for (int i = 0; i < x_values.size(); i++)
            {
                x_total += x_values.get(i) * constants.get(i);
            }

            for (int i = 0; i < result.items.size() && i < 25; i++)
            {
                if (result.items.get(i).id.equals(source_id))
                {
                    System.out.println("Probability from Node : " + (result.probability.get(i) + (constants.get(0) / base)));
                    System.out.println("Manual Probability : " + x_total / base);
                    prediction_id = result.items.get(i).id;
                    answer_index = i;
                    break;
                }
            }

            if (answer_index != -1)
            {
                correct += (1.0 / (answer_index + 1));
                Double remaining = (1.0 - (1.0 / (answer_index + 1)));
                incorrect += remaining;
            }
            else
            {
                none++;
            }

            buffer.write(source_id + ";" + prediction_id + ";" + nodeData);
            buffer.newLine();

            System.out.println(count);
            count++;

            if (x_total > constants.get(0))
            {
                if (result.items.size() > 0)
                {
                    Double y = 1.0;
                    error += Math.pow(y - (x_total / base), 2.0);
                    regression_index++;

                    /*for (int i = 0; i < x_values.size(); i++)
                    {
                        x_values.set(i, x_values.get(i) / base);
                    }*/

                    x_matrix.add(new ArrayList<>());
                    for (int i = 0; i < x_values.size(); i++)
                    {
                        x_matrix.get(x_matrix.size() - 1).add(x_values.get(i));
                    }
                    y_values.add(new ArrayList<>());
                    y_values.get(y_values.size() - 1).add(y);
                    session_length_list.add((double)x_values.size());

                    testBuffer.write(session_id + ";" + source_id + ";");
                    for (int i = 0; i < x_values.size(); i++)
                    {
                        testBuffer.write(x_values.get(i) + "*" + constants.get(i) + "+");
                    }
                    testBuffer.newLine();
                }
            }
        }
        nodes.clear();
        dictionary.clear();

        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Previous : " + (previous / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();

        /*buffer.write("Error Before : " + (error / regression_index));
        buffer.newLine();
        for (int i = 0; i < x_matrix.size(); i++)
        {
            Double total = 0.0;
            for (int j = 0; j < x_matrix.get(i).size(); j++)
            {
                total += x_matrix.get(i).get(j) * constants.get(j);
            }
            buffer.write(total + ";" + y_values.get(i).get(0));
            buffer.newLine();
        }

        //Inverse Method Start
        int mean_session_length = limit;
        boolean invertible = false;
        List<List<Double>> transpose = new ArrayList<>();
        List<List<Double>> interim = new ArrayList<>();
        List<List<Double>> original_interim = new ArrayList<>();
        List<List<Double>> interim_inv = new ArrayList<>();
        List<List<Double>> inverse_check = new ArrayList<>();
        List<List<Double>> result = new ArrayList<>();
        while (mean_session_length > 0 && !invertible)
        {
            System.out.println("Trying length : " + mean_session_length);
            for (int i = 0; i < x_matrix.size(); i++)
            {
                while (x_matrix.get(i).size() < mean_session_length)
                {
                    x_matrix.get(i).add(0.0);
                }
                while (x_matrix.get(i).size() > mean_session_length)
                {
                    x_matrix.get(i).remove(x_matrix.get(i).size() - 1);
                }
            }

            transpose.clear();
            interim.clear();
            original_interim.clear();
            interim_inv.clear();

            System.out.println("Transposing Matrix..");
            TransposeMatrix(x_matrix, transpose);

            System.out.println("Creating Interim..");
            MultiplyMatrix(transpose, x_matrix, interim);

            System.out.println("Inversing Matrix..");
            CopyMatrix(interim, original_interim);
            LoadIdentityMatrix(interim, interim_inv);
            SortMatrix(interim, interim_inv);
            invertible = InverseMatrix(interim, interim_inv);

            if (invertible)
            {
                SaveMatrix(interim, "Before Inverse : ", buffer, false);
                SaveMatrix(interim_inv, "After Inverse : ", buffer, false);
                System.out.println("Inverse found at : " + mean_session_length);

                System.out.println("Checking Inverse..");
                MultiplyMatrix(original_interim, interim_inv, inverse_check);

                interim.clear();
                System.out.println("Multiplying Transpose and Inverse Matrix..");
                MultiplyMatrix(interim_inv, transpose, interim);
                interim_inv.clear();

                System.out.println("Multiplying Interim 2 with Y Matrix..");
                MultiplyMatrix(interim, y_values, result);
            }
            mean_session_length--;
        }

        SaveMatrix(x_matrix, "X Matrix", buffer, false);
        SaveMatrix(interim, "Final Matrix", buffer, false);

        for (int i = 0; i < inverse_check.size(); i++)
        {
            for (int j = 0; j < inverse_check.get(i).size(); j++)
            {
                if (Math.abs(inverse_check.get(i).get(j)) > 0.1f)
                {
                    inverse_check.get(i).set(j, 1.0);
                }
                else
                {
                    inverse_check.get(i).set(j, 0.0);
                }
            }
        }
        SaveMatrix(inverse_check, "Inverse Check : ", buffer, false);

        for (int i = 0; i < result.size(); i++)
        {
            for (int j = 0; j < result.get(i).size(); j++)
            {
                constantsBuffer.write(result.get(i).get(j) + ";");
            }
        }
        constantsBuffer.close();
        //Inverse method end

        error = 0.0;
        for (int i = 0; i < x_matrix.size(); i++)
        {
            Double total = 0.0;
            for (int j = 0; j < x_matrix.get(i).size(); j++)
            {
                total += x_matrix.get(i).get(j) * result.get(j).get(0);
            }
            error += Math.pow(y_values.get(i).get(0) - total, 2.0);
        }

        buffer.write("Error After : " + (error / x_matrix.size()));
        buffer.newLine();

        buffer.newLine();*/
        buffer.close();

        testBuffer.close();
    }

    static public void SaveMatrix(List<List<Double>> matrix, String name, BufferedWriter buffer, boolean size) throws IOException
    {
        DecimalFormat df = new DecimalFormat("#.########");
        buffer.write(name);
        buffer.newLine();
        if (!size)
        {
            for (int i = 0; i < matrix.size(); i++)
            {
                for (int j = 0; j < matrix.get(i).size(); j++)
                {
                    buffer.write(df.format(matrix.get(i).get(j)) + ";");
                }
                buffer.newLine();
            }
        }
        buffer.write(matrix.size() + ";" + matrix.get(0).size());
        buffer.newLine();
    }

    static public void TransposeMatrix(List<List<Double>> matrix, List<List<Double>> temp)
    {
        for (int i = 0; i < matrix.get(0).size(); i++)
        {
            temp.add(new ArrayList<>());
            for (int j = 0; j < matrix.size(); j++)
            {
                temp.get(i).add(matrix.get(j).get(i));
            }
        }
    }

    static public void MultiplyMatrix(List<List<Double>> left, List<List<Double>> right, List<List<Double>> result)
    {
        for (int i = 0; i < left.size(); i++)
        {
            if (result.size() <= i)
            {
                result.add(new ArrayList<>());
            }

            for (int j = 0; j < right.get(0).size(); j++)
            {
                Double total = 0.0;
                for (int k = 0; k < left.get(i).size(); k++)
                {
                    total += left.get(i).get(k) * right.get(k).get(j);
                }

                total = (Math.abs(total) < 0.0001f) ? 0f : total;

                if (result.get(i).size() <= j)
                {
                    result.get(i).add(total);
                }
                else
                {
                    result.get(i).set(j, total);
                }
            }
        }
    }

    static public boolean InverseMatrix(List<List<Double>> matrix, List<List<Double>> result)
    {
        List<List<Double>> temp = new ArrayList<>();
        if (matrix.size() != matrix.get(0).size())
        {
            System.out.println("Unable to inverse the matrix");
            return false;
        }

        //Result = Identity Matrix

        //Temp Matrix
        for (int i = 0; i < matrix.size(); i++)
        {
            temp.add(new ArrayList<>());
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                temp.get(i).add(matrix.get(i).get(j));
            }
        }

        //Zero Triangle
        for (int i = 1; i < temp.size(); i++)
        {
            for (int j = i; j < temp.get(i).size(); j++)
            {
                Double multiplier;
                if (Math.abs(temp.get(i-1).get(i-1)) < 0.00000001f)
                {
                    multiplier = 0.0;
                }
                else
                {
                    multiplier = (temp.get(j).get(i-1) / temp.get(i-1).get(i-1));
                }
                for (int k = 0; k < temp.get(i).size(); k++)
                {
                    temp.get(j).set(k, temp.get(j).get(k) - temp.get(i-1).get(k) * multiplier);
                    result.get(j).set(k, result.get(j).get(k) - result.get(i-1).get(k) * multiplier);
                }
            }
        }

        double determinant = 1.0f;
        for (int i = 0; i < temp.size(); i++)
        {
            System.out.println(temp.get(i).get(i));
            determinant *= temp.get(i).get(i);
        }
        if (!(Math.abs(determinant) > 0))
        {
            return false;
        }
        System.out.println("Determinant : " + determinant);
        //The Remaining Zero(s)
        for (int i = 0; i < temp.size() - 1; i++)
        {
            for (int j = i + 1; j < temp.get(i).size(); j++)
            {
                Double multiplier;
                if (temp.get(j).get(j) == 0)
                {
                    multiplier = 0.0;
                }
                else
                {
                    multiplier = (temp.get(i).get(j) / temp.get(j).get(j));
                }
                for (int k = 0; k < temp.get(i).size(); k++)
                {
                    temp.get(i).set(k, temp.get(i).get(k) - temp.get(j).get(k) * multiplier);
                    result.get(i).set(k, result.get(i).get(k) - result.get(j).get(k) * multiplier);
                }
            }
        }

        //Simplification
        for (int i = 0; i < temp.size(); i++)
        {
            Double multiplier = temp.get(i).get(i);
            for (int j = 0; j < temp.get(i).size(); j++)
            {
                if (multiplier != 0)
                {
                    result.get(i).set(j, result.get(i).get(j) / multiplier);
                }
                else
                {
                    result.get(i).set(j, 0.0);
                }
            }
        }

        return true;
    }

    static public void LoadIdentityMatrix(List<List<Double>> matrix, List<List<Double>> result)
    {
        for (int i = 0; i < matrix.size(); i++)
        {
            if (result.size() <= i)
            {
                result.add(new ArrayList<>());
            }

            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                Double value = (i == j) ? 1.0 : 0.0;
                if (result.get(i).size() <= j)
                {
                    result.get(i).add(value);
                }
                else
                {
                    result.get(i).set(j, value);
                }
            }
        }
    }

    static public void SortMatrix(List<List<Double>> matrix, List<List<Double>> identity)
    {
        for (int k = matrix.get(0).size() - 1; k >= 0; k--)
        {
            for (int i = 0; i < matrix.size(); i++)
            {
                for (int j = i; j < matrix.size(); j++)
                {
                    if (matrix.get(j).get(k) > matrix.get(i).get(k))
                    {
                        List<Double> temp = matrix.get(i);
                        matrix.set(i, matrix.get(j));
                        matrix.set(j, temp);

                        temp = identity.get(i);
                        identity.set(i, identity.get(j));
                        identity.set(j, temp);
                    }
                }
            }
        }
    }

    static public void CullMatrix(List<List<Double>> matrix)
    {
        int abolish_index;
        for (int i = 0; i < matrix.size(); i++)
        {
            if (!(matrix.get(i).get(i) > 0 || matrix.get(i).get(i) < 0))
            {
                abolish_index = i;
                break;
            }
        }

    }

    static public Double GetDeterminant(List<List<Double>> matrix, int row, int column)
    {
        if (matrix.size() == 2)
        {
            return (matrix.get(0).get(0) * matrix.get(1).get(1)) - (matrix.get(0).get(1) * matrix.get(1).get(0));
        }

        Double total = 0.0;
        for (int i = column; i < matrix.get(row).size(); i++)
        {
            total += GetDeterminant(matrix, (row + 1), i);
        }
        return total;
    }

    public static void RecalculateAccuracy(String sourceFile, String resultFile) throws IOException
    {
        BufferedReader source = new BufferedReader(new FileReader(sourceFile));
        FileWriter writer = new FileWriter(resultFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        String source_line;
        String[] source_data;
        Double count = 0.0, correct = 0.0, none = 0.0, previous = 0.0, incorrect = 0.0;
        while ((source_line = source.readLine()) != null)
        {
            if (source_line.contains(";"))
            {
                source_data = source_line.split(";");
                if (source_data[0].contains("["))
                {
                    if (source_data[0].equals(source_data[1]))
                    {
                        System.out.println(source_data[0] + " " + source_data[1]);
                        correct++;
                    }
                    else
                    {
                        if (source_data[1].equals("none"))
                        {
                            none++;
                        }
                        else
                        {
                            incorrect++;
                        }
                    }
                    buffer.write(source_line);
                    buffer.newLine();
                    count++;
                }
            }
        }
        buffer.write("Accuracy : " + (correct / count));
        buffer.newLine();
        buffer.write("None : " + (none / count));
        buffer.newLine();
        buffer.write("Previous : " + (previous / count));
        buffer.newLine();
        buffer.write("Incorrect : " + (incorrect / count));
        buffer.newLine();
        buffer.close();
    }

    public static int FindRegion(String region)
    {
        for (int i = 0; i < nodes.size(); i++)
        {
            if (nodes.get(i).id.equals(region))
            {
                return i;
            }
        }
        return -1;
    }

    public static void ItemTreeMap(String inFile, String outFile) throws IOException
    {
        nodes.clear();
        dictionary.clear();

        String[] data, session;
        String regionID;
        int regionIndex;
        String line;
        Node temp, existing;
        List<String> item_list = new ArrayList<String>();
        int count = 0;
        int window_limit = 11;

        System.out.println("Loading to list..");
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        List<String> multi_order = new ArrayList<>();
        while ((line = reader.readLine()) != null)
        {
            data = line.split(";");
            data = data[1].split("\\|");
            regionID = data[0];

            int window = 1;
            String[] temp_arr = data[1].split("->");

            //System.out.println(line);
            while(window < temp_arr.length && window < window_limit)
            {
                String item_arr = regionID + "|";
                for (int i = 0; i <= temp_arr.length - window; i++)
                {
                    String hotel_id = window + ",action [";
                    for (int j = 0; j < window; j++)
                    {
                        String clean_id = temp_arr[i + j].substring(temp_arr[i + j].indexOf("[") + 1, temp_arr[i + j].indexOf("]"));
                        hotel_id += clean_id;
                        if (j + 1 < window)
                        {
                            hotel_id += ";";
                        }
                    }
                    hotel_id += "]";
                    item_arr += hotel_id;
                    if (i + 1 <= temp_arr.length - window)
                    {
                        item_arr += "->";
                    }
                }
                //System.out.println("Window #" + window + ": ");
                //System.out.println(item_arr);
                multi_order.add(item_arr);
                window++;
            }
            count++;
            System.out.println(count);
        }
        reader.close();

        for (int j = 0; j < multi_order.size(); j++)
        {
            data = multi_order.get(j).split("\\|");
            regionID = data[0];
            regionIndex = FindRegion(regionID);

            if (regionIndex == -1)
            {
                nodes.add(new Node(regionID));
                regionIndex = nodes.size() - 1;
                dictionary.put(regionID, nodes.get(regionIndex));
            }

            data = data[1].split("->");
            temp = nodes.get(regionIndex);

            if (data.length > 0)
            {
                String[] check = data[0].split(",");
                if (!check[0].equals("1"))
                {
                    continue;
                }
            }
            System.out.println(multi_order.get(j));
            item_list.clear();
            for (int i = 0; i < data.length; i++)
            {
                session = data[i].split(",");
                int left_bracket = session[1].indexOf("[");
                int right_bracket = session[1].indexOf("]");
                String relationship = session[1].substring(0, left_bracket - 1);
                String item = session[1].substring(left_bracket + 1, right_bracket);
                String item_combined;
                if (item.matches(".*[a-zA-Z]+.*"))
                {
                    item_combined = "[" + relationship + "," + item + "," + regionID + "]";
                }
                else
                {
                    item_combined = "[" + item + "]";
                }

                if (!(item.equals(regionID) && relationship.equals("search for destination")))
                {
                    item_list.add(item_combined);
                    existing = dictionary.get(item_combined);
                    if (existing == null)
                    {
                        existing = new Node(item_combined);
                        dictionary.put(item_combined, existing);
                    }
                    temp = temp.Register(existing, item_combined, 1.0);
                }
            }
        }

        System.out.println("Mapping graph..");

        System.out.println("Saving graph to file..");
        SaveGraphToFile(outFile);
    }

    public static void LoadFile(String fileName) throws IOException
    {
        content = new ArrayList<String>();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null) {
            content.add(line);
        }
        reader.close();
    }

    public static void LoadConstants(String fileName) throws IOException
    {
        constants.clear();
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));
        while ((line = reader.readLine()) != null)
        {
            if (line.contains(";"))
            {
                String[] values = line.split(";");
                for (String value : values)
                {
                    constants.add(Double.parseDouble(value));
                }
            }
        }
        reader.close();
    }

    public static void LoadMatrix(List<List<Double>> matrix, String fileName) throws IOException
    {
        String line;
        BufferedReader reader = new BufferedReader(new FileReader(fileName));

        while ((line = reader.readLine()) != null)
        {
            matrix.add(new ArrayList<>());
            if (line.contains(";"))
            {
                String[] values = line.split(";");
                for (String value : values)
                {
                    matrix.get(matrix.size() - 1).add(Double.parseDouble(value));
                }
            }
        }
        reader.close();
    }

    public static void CopyMatrix(List<List<Double>> source, List<List<Double>> destination)
    {
        for (int i = 0; i < source.size(); i++)
        {
            if (destination.size() <= i)
            {
                destination.add(new ArrayList<>());
            }

            for (int j = 0; j < source.get(i).size(); j++)
            {
                if (destination.get(i).size() <= j)
                {
                    destination.get(i).add(0.0);
                }
                destination.get(i).set(j, source.get(i).get(j));
            }
        }
    }

    public static void PrintMatrix(List<List<Double>> matrix)
    {
        DecimalFormat df = new DecimalFormat("#.########");
        for (int i = 0; i < matrix.size(); i++)
        {
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                System.out.print(df.format(matrix.get(i).get(j)) + ";");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    public static void SaveGraphToFile(String fileName) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);
        Set<String> keys = dictionary.keySet();

        Node node;
        Double total, average, stdDev;
        for(String key: keys)
        {
            node = dictionary.get(key);
            total = 0.0;
            stdDev = 0.0;

            node.CalculateProbability();
            node.SortProbability();
            for (int i = 0; i < node.items.size(); i++)
            {
                total += node.probability.get(i);
            }

            average = (node.items.size() > 0) ? total / (double)node.items.size() : 0.0;
            for (int i = 0; i < node.items.size(); i++)
            {
                stdDev += Math.pow((node.probability.get(i) - average), 2.0);
            }
            stdDev = (node.items.size() > 0) ? stdDev / node.items.size() : 0;
            stdDev = (Double)Math.pow(stdDev, 0.5);

            buffer.write("#" + node.id + "#");
            buffer.newLine();
            buffer.write("Total : " + total);
            buffer.newLine();
            buffer.write("Average + Std Dev : " + (average + stdDev));
            buffer.newLine();
            for (int i = 0; i < node.items.size(); i++)
            {
                buffer.write(node.items.get(i).id + ";" + node.probability.get(i));
                buffer.newLine();
            }
        }
        buffer.close();
    }

    public static void SessionSort(String fileName, String fileName2, int boundaryTop, int boundaryBottom) throws IOException
    {
        FileWriter writer = new FileWriter(fileName);
        BufferedWriter buffer = new BufferedWriter(writer);

        FileWriter writer2 = new FileWriter(fileName2);
        BufferedWriter buffer2 = new BufferedWriter(writer2);

        String[] arr, point, bites;
        List<String> sessions = new ArrayList<String>();
        List<Integer> index = new ArrayList<Integer>();
        int tempInt, step;
        String tempStr, newContent, lastSeen;

        for (int i = 0; i < content.size(); i++)
        {
            arr = content.get(i).split("\\|");
            sessions.clear();
            index.clear();

            point = arr[1].split("->");
            boolean proceed = true;
            for (int j = 0; j < point.length; j++) {
                if (!point[j].equals("")) {
                    bites = point[j].split(",");
                    try {
                        index.add(Integer.parseInt(bites[0]));
                        sessions.add(bites[1]);
                    } catch (Exception e) {
                        proceed = false;
                        break;
                    }
                }
            }

            if (!proceed) {
                continue;
            }

            for (int j = 0; j < index.size(); j++)
            {
                for (int k = j; k < index.size(); k++)
                {
                    if (index.get(k) < index.get(j))
                    {
                        tempInt = index.get(j);
                        tempStr = sessions.get(j);

                        index.set(j, index.get(k));
                        index.set(k, tempInt);

                        sessions.set(j, sessions.get(k));
                        sessions.set(k, tempStr);
                    }
                }
            }

            newContent = "";
            lastSeen = "";
            step = 1;

            for (int j = 0; j < index.size(); j++)
            {
                if (!sessions.get(j).equals(lastSeen))
                {
                    newContent += step + "," + sessions.get(j);
                    newContent += "->";
                    step++;
                    lastSeen = sessions.get(j);
                }
            }
            newContent = newContent.substring(0, newContent.length() - 2);

            if (i >= boundaryTop && i < boundaryBottom)
            {
                buffer2.write(arr[0] + "|" + newContent);
                buffer2.newLine();
            }
            else
            {
                buffer.write(arr[0] + "|" + newContent);
                buffer.newLine();
            }
        }
        buffer.close();
        buffer2.close();

        content.clear();
    }

    public static void SessionTestFilter(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            data = data[1].split("->");
            for (int i = 0; i < data.length; i++)
            {
                if (data[i].contains("clickout"))
                {
                    temp = session + "|";
                    for (int j = 0; j <= i; j++)
                    {
                        temp += data[j] + "->";
                    }
                    temp = temp.substring(0, temp.length() - 2);
                    buffer.write(temp);
                    buffer.newLine();
                }
            }
        }
        buffer.close();
    }

    public static void SessionTestHide(String inFile, String outFile) throws IOException
    {
        String[] data;
        String line, session, temp;
        BufferedReader reader = new BufferedReader(new FileReader(inFile));
        FileWriter writer = new FileWriter(outFile);
        BufferedWriter buffer = new BufferedWriter(writer);
        while ((line = reader.readLine()) != null)
        {
            data = line.split("\\|");
            session = data[0];
            temp = session + "|";
            data = data[1].split("->");
            for (int i = 0; i < data.length - 1; i++)
            {
                temp += data[i] + "->";
            }
            if (data.length > 1)
            {
                temp = temp.substring(0, temp.length() - 2);
            }
            buffer.write(temp);
            buffer.newLine();
        }
        buffer.close();
    }

    public static void main(String[] args) throws Exception
    {
        long start = System.currentTimeMillis();

        /*Configuration conf = new Configuration();
        conf.set(TextOutputFormat.SEPERATOR, "|");
        FileSystem hdfs = FileSystem.get(conf);
        Job job;

        job = Job.getInstance(conf, "Session Action List");

        job.setJarByClass(MarkovChain.class);
        job.setMapperClass(SessionActionMapper.class);
        job.setCombinerClass(SessionActionReducer.class);
        job.setReducerClass(SessionActionReducer.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path("trivago_train"));
        if (hdfs.exists(new Path("trivago_train_session")))
        {
            hdfs.delete(new Path("trivago_train_session"), true);
        }
        FileOutputFormat.setOutputPath(job, new Path("trivago_train_session"));
        job.waitForCompletion(true);*/

        Double boundaryTop = 0.9;
        Double boundaryBottom = 1.0;
        int fileIndex = 124;

        String session_train, session_test, session_probability, session_test_filtered;
        String session_test_filtered_hidden, session_prediction, session_accuracy;

        //Matrix Function Testing
        List<List<Double>> matrix = new ArrayList<>();
        List<List<Double>> original_matrix = new ArrayList<>();
        List<List<Double>> transpose = new ArrayList<>();
        List<List<Double>> interim = new ArrayList<>();
        List<List<Double>> interim_original = new ArrayList<>();
        List<List<Double>> interim_inv = new ArrayList<>();
        List<List<Double>> interim_check = new ArrayList<>();
        List<List<Double>> y_matrix = new ArrayList<>();
        List<List<Double>> inverse = new ArrayList<>();
        List<List<Double>> multiply = new ArrayList<>();
        List<List<Double>> result = new ArrayList<>();

        LoadMatrix(matrix, "matrix.txt");
        //PrintMatrix(matrix);

        //Check Linear Regression Start
        LoadMatrix(y_matrix, "y_matrix.txt");
        //PrintMatrix(y_matrix);

        Double error = 0.0;
        for (int i = 0; i < matrix.size(); i++)
        {
            Double total = 0.0;
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                total += matrix.get(i).get(j);
            }
            error += Math.pow(y_matrix.get(i).get(0) - total, 2.0f);
        }
        error /= matrix.size();
        System.out.println("Error Before : " + error);

        TransposeMatrix(matrix, transpose);
        //PrintMatrix(transpose);

        MultiplyMatrix(transpose, matrix, interim);
        PrintMatrix(interim);

        LoadIdentityMatrix(interim, inverse);
        PrintMatrix(inverse);

        SortMatrix(interim, inverse);
        //PrintMatrix(interim);
        //PrintMatrix(inverse);

        if (InverseMatrix(interim, inverse))
        {
            System.out.println("Invertible Matrix");
        }
        else
        {
            System.out.println("Non-Invertible Matrix");
        }
        //PrintMatrix(inverse);

        MultiplyMatrix(inverse, transpose, interim);
        //PrintMatrix(interim);

        MultiplyMatrix(interim, y_matrix, result);
        //PrintMatrix(result);

        error = 0.0;
        for (int i = 0; i < matrix.size(); i++)
        {
            Double total = 0.0;
            for (int j = 0; j < matrix.get(i).size(); j++)
            {
                total += matrix.get(i).get(j) * result.get(j).get(0);
            }
            Double single_error = (Double)Math.pow(y_matrix.get(i).get(0) - total, 2.0f);
            error += single_error;
        }
        error /= matrix.size();
        System.out.println("Error After: " + error);
        //Check Linear Regression End

        //Check Everything Start
        /*TransposeMatrix(matrix, transpose);
        PrintMatrix(transpose);

        MultiplyMatrix(transpose, matrix, interim);
        CopyMatrix(interim, interim_original);
        LoadIdentityMatrix(interim, interim_inv);
        SortMatrix(interim, interim_inv);
        InverseMatrix(interim, interim_inv);
        MultiplyMatrix(interim, interim_inv, interim_check);
        PrintMatrix(interim_check);*/
        //Check Everything End

        //Check Multiplication Start
        /*LoadMatrix(y_matrix, "y_matrix.txt");
        PrintMatrix(y_matrix);

        CopyMatrix(matrix, original_matrix);
        PrintMatrix(original_matrix);

        LoadIdentityMatrix(matrix, inverse);
        PrintMatrix(inverse);

        SortMatrix(matrix, inverse);
        PrintMatrix(matrix);
        PrintMatrix(inverse);

        InverseMatrix(matrix, inverse);
        PrintMatrix(inverse);

        MultiplyMatrix(original_matrix, inverse, multiply);
        PrintMatrix(multiply);

        MultiplyMatrix(inverse, y_matrix, result);
        PrintMatrix(result);*/
        //Check Multiplication End

        LoadConstants("constants.txt");

        while (boundaryBottom <= 1.0f)
        {
            LoadFile("trivago_train_session/part-r-00000");
            System.out.println("Iteration : " + fileIndex);

            session_train = "trivago_session/session_train" + fileIndex + ".txt";
            session_test = "trivago_session/session_test" + fileIndex + ".txt";
            session_probability = "trivago_session/probability" + fileIndex + ".txt";
            session_test_filtered = "trivago_session/session_test_filtered" + fileIndex + ".txt";
            session_test_filtered_hidden = "trivago_session/session_test_filtered_hidden" + fileIndex + ".txt";
            session_prediction = "trivago_session/prediction" + fileIndex + ".txt";
            session_accuracy = "trivago_session/accuracy" + fileIndex + ".txt";

            SessionSort(session_train, session_test, (int)(content.size() * boundaryTop), (int)(content.size() * boundaryBottom));
            ItemTreeMap(session_train, session_probability);
            SessionTestFilter(session_test, session_test_filtered);
            SessionTestHide(session_test_filtered, session_test_filtered_hidden);
            CalculateAccuracy(session_test_filtered, session_accuracy);
            boundaryTop += 0.1f;
            boundaryBottom += 0.1f;
            fileIndex++;
            RecalculateAccuracy("trivago_session/accuracy" + (fileIndex - 1) + ".txt", "trivago_session/accuracy" + (fileIndex - 1) + "_recalculated.txt");
        }
        long end = System.currentTimeMillis();
        float sec = (end - start) / 1000F;
        System.out.println(sec + " seconds");
    }
}
