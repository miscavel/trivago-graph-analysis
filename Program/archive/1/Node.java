import java.util.ArrayList;
import java.util.List;

public class Node
{
    public String id;
    public List<Node> items;
    public List<String> relationships;
    public List<Integer> count;
    public List<Float> probability;

    public Node(String _id)
    {
        id = _id;
        items = new ArrayList<Node>();
        relationships = new ArrayList<String>();
        count = new ArrayList<Integer>();
        probability = new ArrayList<Float>();
    }

    void CalculateProbability()
    {
        int totalCount = 0;
        float prob;
        for (int i = 0; i < count.size(); i++)
        {
            totalCount += count.get(i);
        }
        for (int i = 0; i < items.size(); i++)
        {
            prob = (float)count.get(i) / (float)totalCount;
            probability.add(prob);
        }
    }

    int FindCombination(String item, String relationship)
    {
        for (int i = 0; i < items.size(); i++)
        {
            if (items.get(i).id.equals(item) && relationships.get(i).equals(relationship))
            {
                return i;
            }
        }
        return -1;
    }

    public int Register(String item, String relationship)
    {
        int combinationIndex = FindCombination(item, relationship);
        if (combinationIndex == -1)
        {
            items.add(new Node(item));
            relationships.add(relationship);
            count.add(0);
            combinationIndex = items.size() - 1;
        }
        count.set(combinationIndex, count.get(combinationIndex) + 1);
        return combinationIndex;
    }

    public void SortProbability()
    {
        String tempID, tempRelationship;
        int tempCount;
        float tempProbability;
        for (int i = 0; i < items.size(); i++)
        {
            for (int j = i; j < items.size(); j++)
            {
                if (probability.get(j) > probability.get(i))
                {
                    tempID = items.get(i).id;
                    tempRelationship = relationships.get(i);
                    tempCount = count.get(i);
                    tempProbability = probability.get(i);

                    items.get(i).id = items.get(j).id;
                    relationships.set(i, relationships.get(j));
                    count.set(i, count.get(j));
                    probability.set(i, probability.get(j));

                    items.get(j).id = tempID;
                    relationships.set(j, tempRelationship);
                    count.set(j, tempCount);
                    probability.set(j, tempProbability);
                }
            }
        }
    }
}
